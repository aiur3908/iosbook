# 作業八

## 作業範圍：Chapter 1 ~ 8

### 1. 請建立一個專案

並且將頁面背景顏色設置為藍色，接著加入一個UILabel，顯示『Hello, Swift』

### 2. 承上題

請於vidwDidLoad中透過print印製出『Hello, Swift』

請於videDidLoad中，修改view的背景顏色為紅色。

### 3. 請建立一個專案

透過Storyboard，建立多個ViewController，

並且透過UIButton與Action Segue來切換，請至少建立三個頁面。

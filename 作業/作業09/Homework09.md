# 作業九

## 作業範圍：Chapter 1 ~ 9

### 1. 請說明UIViewController畫面顯示時的生命週期是哪些？

### 2. 請說明UIViewController畫面結束時的生命週期是哪些？

### 3. 請建立一個專案

於Storyboard中增加一個UILabel，並且且建立IBOutlet，

接著於程式碼中將UILabel顯示的文字改成『Hello, Swift』



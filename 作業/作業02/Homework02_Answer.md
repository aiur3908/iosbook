# 作業二 - 參考答案

## 作業範圍：Chapter 1 ~ 2

### 1. 試著完成以下的程式碼

有兩個常數，用於紀錄蘋果與香蕉的數量：

```Swift
let apple = 3
let banana = 5
```

請依以上的程式碼為基礎，透過print於終端機中顯示出：「蘋果有3顆，香蕉有5根，總共有8個水果」

參考答案：

```Swift
let apple = 3
let banana = 5
print("蘋果有\(apple)顆，香蕉有\(banana)根，總共有\(apple + banana)個水果")
```

### 2. 以下的程式碼會發生什麼樣的錯誤，為什麼？

```Swift
let number1 = 3
let number2 = 3.0
print(number1 + number2)
```

參考答案：

number1與number2的資料型態不相同，一個為整數，一個則是浮點數，因此沒辦法相加。

### 3. 承上題，那麼該如何修改才可以正常運作呢？

參考答案：

運算時可以將型態轉換成相同的資料型態，例如轉換成浮點數後進行進算：

```Swift
let number1 = 3
let number2 = 3.0
print(Double(number1) + number2)
```

### 4. 請試著將以下的字串轉換成全部大寫。

```Swift
let message = "Hello World!"
```

參考答案：

可以使用uppercased()函式將字串轉換成大寫

```Swift
let message = "Hello World!"
print(message.uppercased())
```

### 5. 承上題，轉換成小寫。

參考答案：

可以使用lowercased()函式將字串轉換成大寫

```Swift
let message = "Hello World!"
print(message.lowercased())
```

### 6. 這邊有兩個常數，試著使用元祖取代它們

```Swift
let firstName = "John"
let lastName = "Smith"
```

你的可以將元祖命名為person，並且有兩個參數，實際使用時會類似以下的樣子：

```Swift
print(person.firstName)
print(person.lastName) 
```

參考答案：

```Swift
let person = (firstName: "John", lastName: "Smith")
print(person.firstName)
print(person.lastName)
```

### 7. 試著使用亂數(Random)，產生隨機一個大樂透號碼(1~49)號

參考答案：

```Swift
let number = Int.random(in: 1...49)
print(number)
```

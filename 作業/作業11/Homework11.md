# 作業十一

## 作業範圍：Chapter 1 ~ 11

### 1.1 請使用Storyboard與Constraints產生以下的畫面

![image](Homework1101_1.png)

最外圍藍色的View：垂直水平置中，且寬高為200

內層白色的View：垂直水平置中，且寬高為藍色View的0.8

右上方紅色的View：靠白色的View右上，且寬高為白色View的0.5

左下方紅色的View：靠白色的View左下，且寬高為白色View的0.5

### 1.2. 承上題，請使用程式碼完成

### 2.1 請使用Storyboard與Constraints產生以下的畫面

![image](Homework1102_1.png)

請自由發揮，中間白色的View寬高均為50，垂直水置中，其他不限。

### 2.2. 承上題，請使用程式碼完成

### 3.1 請使用Storyboard與Constraints產生以下的畫面

![image](Homework1103_1.png)

內容垂直水平置中於畫面中央，寬均為200，紅色與綠色高為100，

而藍色的高度為200，請自由發揮。

### 3.2. 承上題，請使用程式碼完成

### 4.1 請使用Storyboard與Constraints產生以下的畫面

![image](Homework1104_1.png)

紅色的View，垂直置中，距離左右兩邊20，高為200

兩個藍色的View，靠紅色的View左與右，寬高均為100

兩個綠色的View，置中於藍色的View，寬高均為50

### 4.2. 承上題，請使用程式碼完成

# 作業十一

## 作業範圍：Chapter 1 ~ 11

### 1.1. 請使用Storyboard與Constraints產生以下的畫面

![image](Homework1101_1.png)

最外圍藍色的View：垂直水平置中，且寬高為200

內層白色的View：垂直水平置中，且寬高為藍色View的0.8

右上方紅色的View：靠白色的View右上，且寬高為白色View的0.5

左下方紅色的View：靠白色的View左下，且寬高為白色View的0.5

參考答案：

[第1題參考答案](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Homework/Homework11/Homework1101)


### 1.2. 承上題，請使用程式碼完成

參考答案：

```Swift
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //最外圍藍色的View
        let blueView = UIView(frame: .zero)
        blueView.translatesAutoresizingMaskIntoConstraints = false
        blueView.backgroundColor = .blue
        view.addSubview(blueView)
        blueView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        blueView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        blueView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        blueView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        //內層白色的View
        let whiteView = UIView(frame: .zero)
        whiteView.translatesAutoresizingMaskIntoConstraints = false
        whiteView.backgroundColor = .white
        blueView.addSubview(whiteView)
        whiteView.centerXAnchor.constraint(equalTo: blueView.centerXAnchor).isActive = true
        whiteView.centerYAnchor.constraint(equalTo: blueView.centerYAnchor).isActive = true
        whiteView.widthAnchor.constraint(equalTo: blueView.widthAnchor, multiplier: 0.8).isActive = true
        whiteView.heightAnchor.constraint(equalTo: blueView.heightAnchor, multiplier: 0.8).isActive = true

        //右上方紅色的View
        let redView1 = UIView(frame: .zero)
        redView1.translatesAutoresizingMaskIntoConstraints = false
        redView1.backgroundColor = .red
        whiteView.addSubview(redView1)
        redView1.topAnchor.constraint(equalTo: whiteView.topAnchor).isActive = true
        redView1.rightAnchor.constraint(equalTo: whiteView.rightAnchor).isActive = true
        redView1.widthAnchor.constraint(equalTo: whiteView.widthAnchor, multiplier: 0.5).isActive = true
        redView1.heightAnchor.constraint(equalTo: whiteView.heightAnchor, multiplier: 0.5).isActive = true
        
        //右下方紅色的View
        let redView2 = UIView(frame: .zero)
        redView2.translatesAutoresizingMaskIntoConstraints = false
        redView2.backgroundColor = .red
        whiteView.addSubview(redView2)
        redView2.bottomAnchor.constraint(equalTo: whiteView.bottomAnchor).isActive = true
        redView2.leftAnchor.constraint(equalTo: whiteView.leftAnchor).isActive = true
        redView2.widthAnchor.constraint(equalTo: whiteView.widthAnchor, multiplier: 0.5).isActive = true
        redView2.heightAnchor.constraint(equalTo: whiteView.heightAnchor, multiplier: 0.5).isActive = true
    }


}

```

### 2.1. 請使用Storyboard與Constraints產生以下的畫面

![image](Homework1102_1.png)

請自由發揮，中間白色的View寬高均為50，垂直水置中，其他不限。

參考答案：

[第2題參考答案](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Homework/Homework11/Homework1102)

### 2.2. 承上題，請使用程式碼完成

參考答案：

```Swift
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //設置背景的紅色
        view.backgroundColor = UIColor.red
        
        //藍色的View
        let blueView = UIView(frame: .zero)
        blueView.backgroundColor = .blue
        blueView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(blueView)
        blueView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        blueView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        blueView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        blueView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        //綠色的View
        let greenView = UIView(frame: .zero)
        greenView.backgroundColor = .green
        greenView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(greenView)
        greenView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        greenView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        greenView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        greenView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        //白色的View
        let whiteView = UIView(frame: .zero)
        whiteView.backgroundColor = .white
        whiteView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(whiteView)
        whiteView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        whiteView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        whiteView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        whiteView.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }


}

```

### 3.1 請使用Storyboard與Constraints產生以下的畫面

![image](Homework1103_1.png)

內容垂直水平置中於畫面中央，寬均為200，紅色與綠色高為100，

而藍色的高度為200，請自由發揮。

參考答案：

[第3題參考答案](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Homework/Homework11/Homework1103)

### 3.2. 承上題，請使用程式碼完成

參考答案：

```Swift
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //最外層的View
        let contentView = UIView(frame: .zero)
        view.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        contentView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        contentView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        contentView.heightAnchor.constraint(equalToConstant: 400).isActive = true
        
        //紅色的View
        let redView = UIView(frame: .zero)
        redView.backgroundColor = .red
        contentView.addSubview(redView)
        redView.translatesAutoresizingMaskIntoConstraints = false
        redView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        redView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        redView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        redView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        //綠色的View
        let greenView = UIView(frame: .zero)
        greenView.backgroundColor = .green
        contentView.addSubview(greenView)
        greenView.translatesAutoresizingMaskIntoConstraints = false
        greenView.topAnchor.constraint(equalTo: redView.bottomAnchor).isActive = true
        greenView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        greenView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        greenView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        //藍色的View
        let blueView = UIView(frame: .zero)
        blueView.backgroundColor = .blue
        contentView.addSubview(blueView)
        blueView.translatesAutoresizingMaskIntoConstraints = false
        blueView.topAnchor.constraint(equalTo: greenView.bottomAnchor).isActive = true
        blueView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        blueView.rightAnchor.constraint(equalTo: greenView.rightAnchor).isActive = true
        blueView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }

}
```

### 4.1 請使用Storyboard與Constraints產生以下的畫面

![image](Homework1104_1.png)

紅色的View，垂直置中，距離左右兩邊20，高為200

兩個藍色的View，靠紅色的View左與右，寬高均為100

兩個綠色的View，置中於藍色的View，寬高均為50

參考答案：

[第4題參考答案](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Homework/Homework11/Homework1104)

### 4.2. 承上題，請使用程式碼完成

參考答案：

```Swift
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //紅色View
        let redView = UIView(frame: .zero)
        redView.backgroundColor = .red
        redView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(redView)
        redView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        redView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        redView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        redView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        //左邊藍色View
        let leftBlueView = UIView(frame: .zero)
        leftBlueView.backgroundColor = .blue
        leftBlueView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(leftBlueView)
        leftBlueView.leftAnchor.constraint(equalTo: redView.leftAnchor).isActive = true
        leftBlueView.topAnchor.constraint(equalTo: redView.bottomAnchor).isActive = true
        leftBlueView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        leftBlueView.heightAnchor.constraint(equalToConstant: 100).isActive = true

        //左邊綠色View
        let leftGreenView = UIView(frame: .zero)
        leftGreenView.backgroundColor = .green
        leftGreenView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(leftGreenView)
        leftGreenView.centerXAnchor.constraint(equalTo: leftBlueView.centerXAnchor).isActive = true
        leftGreenView.topAnchor.constraint(equalTo: leftBlueView.bottomAnchor).isActive = true
        leftGreenView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        leftGreenView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //右邊藍色View
        let rightBlueView = UIView(frame: .zero)
        rightBlueView.backgroundColor = .blue
        rightBlueView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(rightBlueView)
        rightBlueView.rightAnchor.constraint(equalTo: redView.rightAnchor).isActive = true
        rightBlueView.topAnchor.constraint(equalTo: redView.bottomAnchor).isActive = true
        rightBlueView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        rightBlueView.heightAnchor.constraint(equalToConstant: 100).isActive = true

        //左邊綠色View
        let rightGreenView = UIView(frame: .zero)
        rightGreenView.backgroundColor = .green
        rightGreenView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(rightGreenView)
        rightGreenView.centerXAnchor.constraint(equalTo: rightBlueView.centerXAnchor).isActive = true
        rightGreenView.topAnchor.constraint(equalTo: rightBlueView.bottomAnchor).isActive = true
        rightGreenView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        rightGreenView.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }


}
```

# 作業六 - 參考答案

## 作業範圍：Chapter 1 ~ 6

### 1. 試著練習解包

```Swift
let optionalInt: Int? = 10
```
請使用以下三種解包：
1. 強制解包
2. 可選綁定
3. guard綁定

參考答案：

```Swift
//強制解包
let intValue1 = optionalInt!

//可選綁定
if let intValue2 = optionalInt {
    
}

//guard 綁定
guard let intValue3 = optionalInt else {
    return
}
```

### 2. 試著完成以下的函式

函式將傳入字串陣列，請將字串轉換成整數，並且加總起來，若是無法轉換成整數則忽略。

```Swift
func sum(strings: [String]) -> Int {
        
}
```

參考答案：

```Swift
func sum(strings: [String]) -> Int {
    var result = 0
    for string in strings {
        if let intValue = Int(string) {
            result += intValue
        }
    }
    return result
}
```

### 3. 試著設計一個enum

enum的內容為猜拳的拳種，有剪刀、石頭、布

```Swift
enum RPSType {
    
}
```

參考答案：

```Swift
enum RPSType: Int {
    case rock = 0
    case paper = 1
    case scissors = 2
}
```

### 4. 接上題，請完成以下的函式

該函式可以隨機產生一個猜拳的拳種

```Swift
//隨機產生一個拳種
func randomRPS() -> RPSType {
        
}
```

```Swift
//隨機產生一個拳種
func randomRPS() -> RPSType {
    let random = Int.random(in: 0...2)
    return RPSType(rawValue: random)!
}
```

# 作業六

## 作業範圍：Chapter 1 ~ 6

### 1. 試著練習解包

```Swift
let optionalInt: Int? = 10
```
請使用以下三種解包：
1. 強制解包
2. 可選綁定
3. guard綁定

### 2. 試著完成以下的函式

函式將傳入字串陣列，請將字串轉換成整數，並且加總起來，若是無法轉換成整數則忽略。

```Swift
func sum(strings: [String]) -> Int {
        
}
```

### 3. 試著設計一個enum

enum的內容為猜拳的拳種，有剪刀、石頭、布

```Swift
enum RPSType {
    
}
```

### 4. 接上題，請完成以下的函式

該函式可以隨機產生一個猜拳的拳種

```Swift
//隨機產生一個拳種
func randomRPS() -> RPSType {
        
}
```

# 作業十

## 作業範圍：Chapter 1 ~ 10

### 1. 請透過程式碼產生

一個寬高為200，x,y均為100的UIView，並設置背景顏色為紅色，加入到UIViewController之上

參考答案：

```Swift
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let myView = UIView(frame: CGRect(x: 100,
                                          y: 100,
                                          width: 200,
                                          height: 200))
        myView.backgroundColor = UIColor.red
        view.addSubview(myView)
    }

}
```

### 2. 請透過Storyboard

新增一個UIView，並且設置IBoutlet，於程式碼中將此UIView顏色修改為紅色。

參考答案：

[第2題參考答案](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Homework/Homework10/Homework1002)

### 3. 新增一個UILabel到UIViewController內

方式不限，並且將它的文字顏色修改為紅色。

參考答案：

```Swift
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let myLabel = UILabel(frame: CGRect(x: 100,
                                            y: 100,
                                            width: 100,
                                            height: 100))
        myLabel.text = "Hello"
        myLabel.textColor = UIColor.red
        view.addSubview(myLabel)
    }

}
```

### 4. 請透過程式碼

新增一個UIButton到UIViewController內，並且設置按鈕點擊事件，當使用者點擊時，透過print印製出『Hello』

參考答案：

```Swift
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let myButton = UIButton(frame: CGRect(x: 100,
                                              y: 100,
                                              width: 100,
                                              height: 100))
        myButton.backgroundColor = UIColor.red
        myButton.addTarget(self,
                           action: #selector(buttonTapped),
                           for: .touchUpInside)
        view.addSubview(myButton)
    }
    
    @objc func buttonTapped() {
        print("Hello")
    }

}
```

### 5. 承上題

請改使用Storyboard與IBAction完成。

參考答案：

[第5題參考答案](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Homework/Homework10/Homework1005)

### 6. 請新增一個專案

接著增加UISwitch與UIView，當UISwift開關為開時，將UIView隱藏，反之則顯示。

參考答案：

[第6題參考答案](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Homework/Homework10/Homework1006)

### 7. 請新增一個專案

接著增加兩個UITextField，可讓使用者輸入帳號與密碼，

帳號的UITextFiled必須設置Placeholder，提示使用者輸入帳號，

密碼的UITextField必須設置Placehoder，提示使用者輸入密碼，且為密碼樣式。

接著新增一個UIButton，點擊時會印製出使用者所輸入的帳號與密碼。

參考答案：

[第7題參考答案](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Homework/Homework10/Homework1007)


### 8. 試著使用UITextView

請顯示大量的文字內容，使用者不可更改裡面的文字。

參考答案：

[第8題參考答案](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Homework/Homework10/Homework1008)

### 9. 請新增一個專案

接著增加UISegmentedControl，上面顯示紅綠藍，以及新增一個UIView，

隨著使用者切換Segment時，會一併變化UIView的背景顏色，預設選擇為紅色。

參考答案：

[第9題參考答案](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Homework/Homework10/Homework1009)

### 10.  請新增一個專案

接著增加一個UISlider與UILabel，

該UISlider可調整的範圍為1~100，而UILabel顯示該數值。

參考答案：

[第10題參考答案](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Homework/Homework10/Homework1010)

### 11. 請新增一個專案

接著增加一個UIStepper與UILabel，

該UIStepper可選擇的範圍為1~100，且可按著按鈕的遞增減，UILabel顯示該數值。

參考答案：

[第11題參考答案](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Homework/Homework10/Homework1011)

### 12.  請新增一個專案

接著新增一張圖片到Assets內，並於UIImageView顯示它。

參考答案：

[第12題參考答案](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Homework/Homework10/Homework1012)

### 13.  請新增一個專案

接著於Assets中新增一個顏色，並於程式碼中使用它。

參考答案：

[第13題參考答案](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Homework/Homework10/Homework1013)

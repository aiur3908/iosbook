# 作業四 - 參考答案

## 作業範圍：Chapter 1 ~ 4

### 1. 請參考以下的程式碼

```Swift
var a = 1
a = a + 2
```
請將「 a = a + 2 」修改成複合賦值運算子，將整體語法優化。

參考答案：

```Swift
var a = 1
a += 2
```

### 2. 梯形面積的公式為：(上底 + 下底) * 高 / 2
這邊我們提供上底、下底與高的數值，請你完成梯形面積的計算：

```Swift
let top = 4 //上底
let bottom = 6 //下底
let height = 5 //高
let area = ????
```

參考答案：

```Swift
let top = 4 //上底
let bottom = 6 //下底
let height = 5 //高
let area = (top + bottom) * height / 2
```

### 3. 我們有一個變數用於儲存使用者的成績

```Swift
let score = 93
```
請利用此變數搭配條件判斷式(if)，根據分數的不同print出不同的評語

| 條件           | 評語          | 
| ------------- |:-------------:|
|100分|太厲害| 
|90分以上|很棒哦| 
|60分以上|及格囉| 
|不滿60分|要好好加油哦| 

參考答案：

```Swift
let score = 93
if score == 100 {
    print("太厲害")
} else if score >= 90 {
    print("很棒哦"")
} else if score >= 60 {
    print("及格囉")
} else {
    print("要好好加油哦")
}
```

### 4. 試著完成這段條件判斷式，我們希望判別該變數是奇數還是偶數

```Swift
let number = 3
if ???? {
    print("number是偶數")
} else {
    print("number是奇數")
}
```
參考答案：

```Swift
let number = 3
if number % 2 == 0 {
    print("number是偶數")
} else {
    print("number是奇數")
}
```

### 5. 試著使用三元運算子完成以下的程式碼，若是age >= 18時，price為500，否則則為250

```Swift
let age = 18
let price = ?????
```

參考答案：

```Swift
let age = 18
let price = age >= 18 ? 500 : 250
```

### 6. 假設有四個變數如下
分別是年齡(age)、性別(gender)、身高(height)、體重(weight)，假設同時符合以下的條件的話就是真命天子：

* 年齡(age) 大於18歲
* 性別(gender)為男
* 身高(height)大於180公分
* 體重(weight)少於80公斤

當所有條件都成立時，就是真命天子，試著使用條件運算式將真命天子的判斷完成：

```Swift
let age = 20
let gender = "男"
let height = 180
let weight = 70

if ???? {
    print("真命天子！")
}
```

參考答案：

```Swift
let age = 20
let gender = "男"
let height = 180
let weight = 70

if age > 18 &&
   gender == "男" &&
    height >= 180 &&
    weight <= 80 {
    print("真命天子！")
}
```

### 7. 試著完成此條件判斷式，這個條件判斷式會判斷出該年份是否為閏年

```Swift
let year = 2000

if ???? {
    print("\(year) 是閏年哦")
}
```
閏年的定義為：
1. 能被400整除的年份
2. 能被4整除，但是不能被100整除的年份

參考答案：

```Swift
let year = 2000
if (year % 4 == 0 && year % 100 != 0) ||
    year % 400 == 0 {
    print("\(year) 是閏年哦")
}
```

### 8. 試著使用For - in 遍歷以下的陣列

```Swift
let numbers = [1,2,5,7,12,30]
```
接著判斷該數值是奇數還是偶數，接著透過print來輸出結果。

參考答案：

```Swift
let numbers = [1,2,5,7,12,30]
for number in numbers {
    if number % 2 == 0 {
        print("\(number)是偶數")
    } else {
        print("\(number)是奇數")
    }
}
```

### 9. 試著使用迴圈將九九乘法表透過print輸出。

參考答案：

```Swift
for i in 1...9 {
    for j in 1...9 {
        print("\(i) x \(j) = \(i * j)")
    }
}
```

### 10. 設你現在要設計撲克牌遊戲，試者使用列舉來定義不同的花色

```Swift
enum PokerSuitType {
    
}
```

參考答案：

```Swift
enum PokerSuitType {
    case spade //黑桃
    case heart //紅心
    case diamond //方塊
    case club //梅花
}
```

### 11. 承上題
請使用你定義的花色列舉，創建一個實體，並且使用switch case來判斷當前花色為何，使用print輸出花色內容到終端機之中。

參考答案：

```Swift
let poker = PokerSuitType.diamond
switch poker {
case .spade:
    print("黑桃")
case .heart:
    print("紅心")
case .diamond:
    print("方塊")
case .club:
    print("梅花")
}
```

### 12. 承上題
假設我們的花色的大小是使用RawValue來定義，請為你的列舉增加RawValue，且依照花色大小排序。

參考答案：

```Swift
enum PokerSuitType: Int {
    case club = 0
    case diamond = 1
    case heart = 2
    case spade = 3
}
```

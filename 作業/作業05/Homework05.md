# 作業五

## 作業範圍：Chapter 1 ~ 5

### 1. 試著建立一個函式，該函式不需要傳入值與回傳值，內容只需要print("Hello World!")

### 2. 試著建立一個函式，可以傳入兩個數值，接著進行比較，將較大的值透過return傳出。

### 3. 試著建立一個函式，該函式可以隨機傳出一個骰子數(1~6)。

### 4. 試著完成這個函式，傳入一組數值陣列，並找出該陣列最大的值為何。

```Swift
func findMaxValue(in numbers: [Int]) -> Int {

}
```

### 5. 試著完成這兩個函式，攝氏轉華式與華式轉攝氏。

```Swift
///攝氏轉華氏
func celsiusToFahrenheit(_ celsius: Double) -> Double {

}

///華氏轉攝氏
func fahrenheitToCelsius(_ fahrenheit: Double) -> Double {

}
```

### 6. 試著完成這兩個函式，傳入一個整數陣列後，可以找出陣列內哪些是偶數、奇數。

```Swift
//找到全部的偶數
func findEvenNumbers(_ numbers: [Int]) -> [Int] { 

}

//找到全部的奇數
func findOddNumbers(_ numbers: [Int]) -> [Int] {

}
```


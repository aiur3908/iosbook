# 作業五 - 參考答案

## 作業範圍：Chapter 1 ~ 5

### 1. 試著建立一個函式，該函式不需要傳入值與回傳值，內容只需要print("Hello World!")

參考答案：

```Swift
func sayHelloWorld() {
    print("Hello World!")
}
```

### 2. 試著建立一個函式，可以傳入兩個數值，接著進行比較，將較大的值透過return傳出。

參考答案：

```Swift
func max(_ num1: Int, _ num2: Int) -> Int {
    return num1 > num2 ? num1 : num2
}
```

### 3. 試著建立一個函式，該函式可以隨機傳出一個骰子數(1~6)。

參考答案：

```Swift
func getRandomPoint() -> Int {
    return Int.random(in: 1...6)
}
```

### 4. 試著完成這個函式，傳入一組數值陣列，並找出該陣列最大的值為何。

```Swift
func findMaxValue(in numbers: [Int]) -> Int {

}
```

參考答案：

```Swift
func findMaxValue(in numbers: [Int]) -> Int {
    return numbers.sorted(by: >)[0]
}
```

### 5. 試著完成這兩個函式，攝氏轉華式與華式轉攝氏。

```Swift
///攝氏轉華氏
func celsiusToFahrenheit(_ celsius: Double) -> Double {

}

///華氏轉攝氏
func fahrenheitToCelsius(_ fahrenheit: Double) -> Double {

}
```

參考答案：

```Swift
///攝氏轉華氏
func celsiusToFahrenheit(_ celsius: Double) -> Double {
    return celsius * (9 / 5) + 32
}

///華氏轉攝氏
func fahrenheitToCelsius(_ fahrenheit: Double) -> Double {
    return (fahrenheit - 32) * (5 / 9)
}
```

### 6. 試著完成這兩個函式，傳入一個整數陣列後，可以找出陣列內哪些是偶數、奇數。

```Swift
//找到全部的偶數
func findEvenNumbers(_ numbers: [Int]) -> [Int] { 

}

//找到全部的奇數
func findOddNumbers(_ numbers: [Int]) -> [Int] {

}
```

參考答案：

```Swift
//找到全部的偶數
func findEvenNumbers(_ numbers: [Int]) -> [Int] {
    var result: [Int] = []
    for number in numbers {
        if number % 2 == 0 {
            result.append(number)
        }
    }
    return result
}

//找到全部的奇數
func findOddNumbers(_ numbers: [Int]) -> [Int] {
    var result: [Int] = []
    for number in numbers {
        if number % 2 != 0 {
            result.append(number)
        }
    }
    return result
}
```


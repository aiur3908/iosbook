# 作業七

## 作業範圍：Chapter 1 ~ 7

### 1. 請設計一個學生(Student)的類別
擁有以下的屬性：學號(studentID)、姓名(name)、年齡(age)

### 2. 承上題
因為大部分學生都為18歲，因此試著讓年齡固定為18歲。

### 3. 承上題
你想要額外增加一個屬性，用於記錄學生的暱稱(nickname)，但是不是每個學生都有暱稱，請問該屬性該如何宣告。

### 4. 假設你現在設計一款遊戲，並且設計了兩個種族
精靈(Elf)，獸人(Orc)，他們擁有力量(strength)與敏捷(agility)屬性。
並且擁有普通攻擊(attack)與種族攻擊兩種攻擊模式。

普通攻擊的計算為 (力量 + 敏捷) / 2
種族攻擊則依照種族的不同，有不同的計算方式。

因此你設計了以下的類別：

```Swift
class Elf {
    var strength: Double
    var agility: Double
    
    init(strength: Double, agility: Double) {
        self.strength = strength
        self.agility = agility
    }
    
    func attack() -> Double {
        return (strength + agility) / 2.0
    }
    
    func elfAttack() -> Double {
        return agility * 2
    }
}


class Orc {
    var strength: Double
    var agility: Double
    
    init(strength: Double, agility: Double) {
        self.strength = strength
        self.agility = agility
    }
    
    func attack() -> Double {
        return (strength + agility) / 2.0
    }
    
    func orcAttack() -> Double {
        return strength * 2
    }
}
```

此時，你應該會發現有許多相似的部分，你想要進行重構，將相同的部分抽象成另外一個Class，讓這兩個種族去繼承他。

大略程式結構如下，請試著完成它。

```Swift
class Race {

}

class Elf: Race {

}

class Human: Race {

}
```

### 5. 承上題，此時你想增加額外兩個職業
精靈弓箭手(ElfArcher)，擁有弓箭攻擊的技能(bowAttack)，計算傷害為敏捷 * 1.5。

獸人戰士(OrcWarrior)，擁有長劍攻擊的技能(swordAttack)，傷害計算為力量 *1.5。

請透過以上資訊增加兩個Class。

### 6. 承上題，接下來你想設計一些關卡。
你想到透過函式來設計關卡，舉例來說，關卡01，必須是獸人才能進入，函式結構就必須如下：

```Swift
func stage01(player: Orc) {
    //進行攻擊
    player.attack()
}
```

依照這個設計模式，請設計以下的關卡：

stage1： 所有種族均可進入
stage2： 只有獸人戰士可進入
stage3： 只有精靈可進入

```Swift
func stage1(player: ???) {

}

func stage2(player: ???) {

}

func stage3(player: ???) {

}
```

### 7. 承上題，請修改關卡3的一些處理
增加判斷，如果進入的是精靈弓箭手，使用弓箭攻擊，
如果進入的是精靈，則使用精靈攻擊。

### 8. 請說明Class與Struct的差異

### 9. 請說明值類型與參考類型

# 作業三 - 參考答案

## 作業範圍：Chapter 1 ~ 3

### 1. 請參考以下的程式碼，這邊有一個整數陣列

```Swift
var numbers = [30, 55, 27]
```

請試著操作這個陣列

1. 刪除index為1的元素
2. 新增一個數字到這個陣列之中

參考答案：

```Swift
var numbers = [30, 55, 27]
numbers.remove(at: 1)
numbers.append(22)
print(numbers)
```

### 2. 請考以下的程式碼，請將兩個陣列合併

```Swift
var number1 = [3, 20, 33]
var number2 = [2, 10]
```
參考答案：

```Swift
var number1 = [3, 20, 33]
var number2 = [2, 10]
var number3 = number1 + number2
print(number3)
```

### 3. 請參考以下的程式碼

```Swift
var numbers = [33, 27, 33, 80, 27]
```

請將這個陣列相同的元素移除，並且排序由大而小。

參考答案：

```Swift
var numbers = [33, 27, 33, 80, 27]
//將陣列轉換成Set，去除重複的部分
var numbersSet = Set(numbers)
//將Set轉換成Array
var newNumbers = Array(numbersSet)
//將Array排序
numbers = newNumbers.sorted(by: <)
print(numbers)
```
這題如果熟悉一些，可以將整串操作結合起來：

```Swift
var numbers = [33, 27, 33, 80, 27]
numbers = Array(Set(numbers)).sorted(by: <)
```

### 4.  我們有一個整數陣列如下：

```Swift
var numbers = [3,4,8,1,9]
```
請問該如何找到這個整數陣列中的最小值與最大值？

參考答案：

```Swift
var numbers = [3,4,8,1,9]
let max = numbers.sorted(by: >)[0]
let min = numbers.sorted(by: <)[0]
```

### 5. 請建立一個Key為字串，Value為整數的Dictionary
並且練習新增與刪除功能。

參考答案：

```Swift
var dictionary: [String: Int] = ["A": 0,
                                 "B": 1]
dictionary["C"] = 2
dictionary.removeValue(forKey: "A")
```

### 6. 請參考以下的程式碼

```Swift
var dictionary = ["A": "蘋果",
                  "B": "香蕉"]
```

請操作這個字典：

1. 新增一個Key為C，而Value為貓咪的內容
2. 將Key為A的Value改成動物這個字串

參考答案：

```Swift
var dictionary = ["A": "蘋果",
                  "B": "香蕉"]
dictionary["C"] = "貓咪"
dictionary["A"] = "動物"
```

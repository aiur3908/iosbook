# Chapter 15

## 擴展(Extension)

### 擴展基本架構

```Swift
extension SomeType { 
    // 新增的功能
}
```

### 透過擴展於String增加一個函式

```Swift
extension String {
    func toIntValue() -> Int? {
        return Int(self)
    }
}

let string = "123"
let intValue = string.toIntValue()
```

### 透過擴展增加計算屬性

```Swift
extension Double {
    var km: Double { return self * 1000.0 }
    var m: Double { return self }
    var cm: Double { return self / 100.0 }
}

let distance = 30.km + 100.m
// 距離為 30100.00 公尺
print(" 距離為 \(distance) 公尺 ")
```

### 透過擴展增加內嵌類型

```Swift
extension Int { 
    enum Kind {
        case negative, zero, positive
    }
    
    var kind: Kind { 
        switch self {
        case 0:
            return .zero
        case let x where x > 0:
            return .positive
        default:
            return .negative
        }
    } 
}

let number = 11 
switch number.kind { 
    case .zero:
        print("number 是 0")
    case .positive:
        print("number 是整數 ") 
    case .negative:
        print("number 是負數 ")
}
```

## 協議(Protocol)

### 官方文件
[Protocol](https://docs.swift.org/swift-book/LanguageGuide/Protocols.html)

### 協議基本架構

```Swift
protocol SomeProtocol { 
    //協議的定義
}
```

### 實作協議

```Swift
class SomeClass: AProtocol, BProtocol { 

}
```

### 繼承類別與實作協議

```Swift
class SomeClass: SomeSuperClass, AProtocol, BProtocol {

}
```

### 透過擴展來實作協議

```Swift
extension SomeClass: AProtocol { 

}
```

### 規定協議只能由Class來實作

```Swift
protocol SomeProtocol: AnyObject { 

}
```

### 屬性要求的協議與實作

```Swift
protocol AProtocol {
    var a: Int { get set }
    var b: Int { get }
}

struct A: AProtocol { 
    var a: Int
    var b: Int
}
```

### 函式要求的協議與實作

```Swift
protocol SomeProtocol {
    static func someTypeMethod()
    func someMethod()
}

class SomeClass: SomeProtocol {
    static func someTypeMethod() {
    
    }
    
    func someMethod() {
    
    }
}
```

### 擁有可選函式協議與實作

```Swift
@objc protocol SomeProtocol {
    func someMethod()
    @objc optional func optionalMethod()
}

class SomeClass: SomeProtocol {
    func someMethod() { 
    
    }
}
```

### 範例01
自定義DataSource與Delegate

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter15/Example1501)

程式碼

MyButton.swift

```Swift
import UIKit

protocol MyButtonDataSource: AnyObject {
    func background(in myButton: MyButton) -> UIColor
}

@objc protocol MyButtonDelegate: AnyObject {
    @objc func myButtonDidTapped(_ myButton: MyButton)
}

class MyButton: UIButton {

    weak var dataSource: MyButtonDataSource?
    weak var delegate: MyButtonDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAction()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupAction()
    }
    
    private func setupAction() {
        addTarget(self, action: #selector(tapped), for: .touchUpInside)
    }
    
    @objc private func tapped() {
        delegate?.myButtonDidTapped(self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = dataSource?.background(in: self)
    }

}
```

ViewController.swift

```Swift
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let myButton = MyButton(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
        myButton.dataSource = self
        myButton.delegate = self
        view.addSubview(myButton)
    }
}

extension ViewController: MyButtonDataSource {
    func background(in myButton: MyButton) -> UIColor {
        return .red
    }
}

extension ViewController: MyButtonDelegate {
    func myButtonDidTapped(_ myButton: MyButton) {
        print("MyButton 被點擊囉 ")
    }
}

```

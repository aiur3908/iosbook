# UIDatePicker

### 官方文件
[UIDatePicker](https://developer.apple.com/documentation/uikit/uidatepicker)

### 範例01
基本使用方式

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter18/UIDatePicker/UIDatePickerExample01)

程式碼

```Swift
class ViewController: UIViewController {

    @IBOutlet var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDatePicker()
    }
    
    private func setupDatePicker() {
        // 最小日期為今天
        let minDate = Date()
        datePicker.minimumDate = minDate
        // 最大日期為今天加上一年
        let maxDate = Calendar.current.date(byAdding: .year, value: 1, to: Date())
        datePicker.maximumDate = maxDate
    }

    @IBAction func buttonTapped(_ sender: Any) {
        let date = datePicker.date
        print(date)
    }
    
}
```

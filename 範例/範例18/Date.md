# Date

### 官方文件
[Date](https://developer.apple.com/documentation/foundation/date)

### 取得當前時間

```Swift
let date = Date()
```

### 透過DateFormatter轉換Date

```Swift
let dateFormatter = DateFormatter() 
dateFormatter.dateFormat = "yyyy/MM/dd"

let dateString = dateFormatter.string(from: date)
print(dateString)
```
### 透過Unix時間來產生Date

```Swift
let date = Date(timeIntervalSince1970: 1)
```
### 將字串轉換成Date

```Swift
let dateFormatter = DateFormatter() 
dateFormatter.dateFormat = "yyyy/MM/dd"
if let date = dateFormatter.date(from: "2020/10/01") {
    print(date)
}
```
### 透過DateFormatter轉換成字串

```Swift
let date = Date()
let dateFormatter = DateFormatter() 
dateFormatter.dateFormat = "yyyy" 
print(dateFormatter.string(from: date))
```

```Swift
let date = Date()
let dateFormatter = DateFormatter() 
dateFormatter.dateFormat = "yyyy/MM/dd HH:mm" 
print(dateFormatter.string(from: date)) //2020/09/01 12:00
```

```Swift
let date = Date()
let dateFormatter = DateFormatter()
dateFormatter.dateFormat = "yyyy 年 MM 月 dd 日 HH 時 mm 分 ss 秒 " print(dateFormatter.string(from: date))
```

### 透過Locale轉換成對應的語系

```Swift
let date = Date()
let dateForamtter = DateFormatter()
dateForamtter.dateFormat = "MMMM"
dateForamtter.locale = Locale(identifier: "en")
print(dateForamtter.string(from: date))
```

### 透過Calendar來增減時間

```Swift
let date = Date()
// 取得使用者的日曆
let calendar = Calendar.current
// 當前日期加上三天
let newDate = calendar.date(byAdding: .day, value: 3, to: date)
```

### 透過Calendar來判斷相差幾天

```Swift
let dateFormatter = DateFormatter() 
dateFormatter.dateFormat = "yyyy/MM/dd"
if let date1 = dateFormatter.date(from: "2020/01/01"),
   let date2 = dateFormatter.date(from: "2020/10/01") { 
   let calendar = Calendar.current
   let dateComponents = calendar.dateComponents([.day], from: date1, to: date2) 
   print(dateComponents.day!)
}
```

### 透過Calendar來判斷相差幾個月又幾天

```Swift
let dateFormatter = DateFormatter() 
dateFormatter.dateFormat = "yyyy/MM/dd"
if let date1 = dateFormatter.date(from: "2020/01/01"),
   let date2 = dateFormatter.date(from: "2020/10/01") { 
    let calendar = Calendar.current
    let dateComponents = calendar.dateComponents([.month, .day], from: date1, to: date2) 
    print(dateComponents.month!)
    print(dateComponents.day!)
}
```

### 判斷兩個日期的大小

```Swift
if date1 > date2 { 
    print("date1 大於 date2")
}

if date1 == date2 { 
    print("date1 等於 date2")
}

if date1 < date2 { 
    print("date1 小於 date2")
}
```

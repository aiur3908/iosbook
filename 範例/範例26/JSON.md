# JSON

## JSONSerialization

### 官方文件
[JSONSerialization](https://developer.apple.com/documentation/foundation/jsonserialization)

### 透過JSONSerialization將JSON轉換成Dictionary

```Swift

let jsonString = """
{
"a": "a",
"b": 1, 
"c": 1.1
}
"""

if let jsonData = jsonString.data(using: .utf8) { 
    do {
        if let dictionary = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any] {
            print(dictionary["a"]!)
        }
    } catch { 
        print(error.localizedDescription)
    } 
}

```

### 透過JSONSerialization將Dictionary轉換回JSON字串

```Swift
do {
    let dictionary: [String: Any] = ["a": "a", "b": 1, "c": 1.1]
    // 透過 JSONSerialization 將 Dictionary 轉換成 Data
    let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: [])
    // 將 Data 轉換成 String
    if let jsonString = String(data: jsonData, encoding: .utf8) {
        print(jsonString) 
    }
} catch { 
    print(error.localizedDescription)
}
```

## Codable

### 官方文件
[Codable](https://developer.apple.com/documentation/swift/codable)

### 透過JSONDecoder將JSON轉換成物件

```Swift
struct MyJSONModel: Codable {
    var a: String
    var b: Int
    var c: Double
}

let jsonString = """ 
{
"a": "a", 
"b": 1, 
"c": 1.1
}
"""

if let jsonData = jsonString.data(using: .utf8) { 
    do {
        let jsonDecoder = JSONDecoder()
        let myJSONModel = try jsonDecoder.decode(MyJSONModel.self, from: jsonData)
        print(myJSONModel.a) //a 
        print(myJSONModel.b) //1 
        print(myJSONModel.c) //1.1
    } catch { 
        print(error.localizedDescription)
    }
}
```

### 透過JSONDecoder將物件轉換成JSON

```Swift
struct MyJSONModel: Codable {
    var a: String
    var b: Int
    var c: Double
}

do {
    let myJSONModel = MyJSONModel(a: "123", b: 2, c: 3.5)
    let jsonEncoder = JSONEncoder()
    let jsonData = try jsonEncoder.encode(myJSONModel)
} catch { 
    print(error.localizedDescription)
}
```

### CodingKey

```Swift
struct User: Codable { 
    var name: String 
    var age: Int
    
    enum CodingKeys: String, CodingKey { 
        case name = "Name"
        case age = "Age"
    }
}


let jsonString = """
{
"Name": "Jerry",
"Age": 18
}
"""

if let jsonData = jsonString.data(using: .utf8) { 
    do {
        let jsonDecoder = JSONDecoder()
        let user = try jsonDecoder.decode(User.self, from: jsonData)
        print(user.name) //Jerry
        print(user.age) //18
    } catch { 
        print(error.localizedDescription)
    }
}
```
### DecodingError

[DecodingError](https://developer.apple.com/documentation/swift/decodingerror)

```Swift
do {
    let jsonDecoder = JSONDecoder()
    let user = try jsonDecoder.decode(User.self, from: jsonData)
} catch DecodingError.keyNotFound(let key, let context) { 
    print("keyNotFound key = \(key), context: \(context)")
} catch DecodingError.typeMismatch(let type, let context) { 
    print("typeMismatch type = \(type), context: \(context)")
} catch DecodingError.valueNotFound(let type, let context) { 
    print("valueNotFound type = \(type), context: \(context)")
} catch {
    print("其他錯誤 \(error.localizedDescription)")
}
```

### JSON內有其他JSON結構

```JSON
{
"status": 0, 
"data": {
    "userName": "Jerry",
    "age": 30 
 }
}
```
```Swift
struct UserRespose: Codable { 
    var status: Int
    var data: Data
    struct Data: Codable { 
        var userName: String 
        var age: Int
    }
}
```

### 整個JSON是一個陣列

```JSON
[
  {
    "name": "小明",
    "score": 91
  },
  {
    "name": "小華",
    "score": 82
  },
  {
    "name": "小花",
    "score": 84
  }
]
```

```Swift
struct Student: Codable { 
    var name: String
    var score: Int
}

let students = try jsonDecoder.decode([Student].self, from: jsonData)
```

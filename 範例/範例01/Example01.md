# Chapter 1 

### 蘋果官方網站
[Apple](https://www.apple.com)

### 蘋果開發者網站
[蘋果開發者網站](https://developer.apple.com/)

### Swift開源網址
[Swift開源網址](https://github.com/apple/swift)

### Hello world

```Swift
import UIKit

var str = "Hello, world"
print(str)
```

### 多行程式碼

```Swift
var str = "Hello, world" 
print(str)
str = "Hello, swift"
print(str)
```

### 註解程式碼

```Swift
var str1 = "Hello1" 
//var str2 = "Hello2"
```


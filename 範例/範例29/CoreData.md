# CoreData

### 官方文件
[CoreData](https://developer.apple.com/documentation/coredata)

### 取得Context

```Swift
// 取得 AppDelegate 實體
let appDelegate = UIApplication.shared.delegate as? AppDelegate 
// 取得 PersistentContainer
let persistentContainer = appDelegate?.persistentContainer
// 取得 Context
let context = persistentContainer?.viewContext
```

### 範例01
新增

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter29/CoreData/CoreDataExample01)

```Swift
if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
    let persistentContainer = appDelegate.persistentContainer
    let context = persistentContainer.viewContext
    if let student = NSEntityDescription.insertNewObject(forEntityName: "Student",
                                                         into: context) as? Student {
        student.name = "Jerry"
        student.age = 30
        student.id = "110123001"
        do {
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }
}
```

### 讀取資料

建立請求

```Swift
let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Student")
```

完整程式碼

```Swift
if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
    let persistentContainer = appDelegate.persistentContainer
    let context = persistentContainer.viewContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Student")
    do {
        let students = try context.fetch(request) as? [Student]
    } catch {
        print(error.localizedDescription)
    }
}
```

### 修改

```Swift
if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
    let persistentContainer = appDelegate.persistentContainer
    let context = persistentContainer.viewContext
    //建立請求
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Student")
    //搜尋name = Jerry的資料
    let predicate = NSPredicate(format: "name = 'Jerry'")
    request.predicate = predicate
    do {
        if let students = try context.fetch(request) as? [Student],
           let jerry = students.first {
            jerry.age = 31
            //修改資料後儲存
            try context.save()
        }
    } catch {
        print(error.localizedDescription)
    }
}
```

### 刪除

```Swift
if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
    let persistentContainer = appDelegate.persistentContainer
    let context = persistentContainer.viewContext
    //建立請求
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Student")
    //搜尋name = Jerry的資料
    let predicate = NSPredicate(format: "name = 'Jerry'")
    request.predicate = predicate
    do {
        if let students = try context.fetch(request) as? [Student],
           let jerry = students.first {
            //刪除該資料
            context.delete(jerry)
            //保存
            try context.save()
        }
    } catch {
        print(error.localizedDescription)
    }
}

```

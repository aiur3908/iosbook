# FileManager

### 官方文件
[FileManager](https://developer.apple.com/documentation/foundation/filemanager)

### 取得Document路徑

```Swift
let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
```

### 新增資料夾

```Swift
if let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
    let directoryURL = url.appendingPathComponent("Hello")
    do {
        try FileManager.default.createDirectory(at: directoryURL,
                                                withIntermediateDirectories: true,
                                                attributes: nil)
    } catch {
        print(error.localizedDescription)
    }
}
```

### 新增檔案

```Swift
if let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
    let path = url.path.appending("/Hello.txt")
    let content = "Hello World!"
    let data = content.data(using: .utf8)
    FileManager.default.createFile(atPath: path, contents: data, attributes: nil)
}
```

### 複製、刪除、移動

```Swift
// 複製
try FileManager.default.copyItem(atPath: path, toPath: newPath) 
// 移動
try FileManager.default.moveItem(atPath: path, toPath: newPath) 
// 刪除
try FileManager.default.removeItem(atPath: path)
```

### 寫入檔案

```Swift
let text = "Hello World"
try text.write(toFile: path, atomically: true, encoding: .utf8)
```

### 確認檔案是否存在

```Swift
FileManager.default.fileExists(atPath: path)
```

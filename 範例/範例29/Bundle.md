# Bundle

### 官方文件
[Bundle](https://developer.apple.com/documentation/foundation/bundle)

### 範例01
讀取儲存於Bundle內的資料

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter29/Bundle/BundleExample01)

程式碼

```Swift
do {
    if let path = Bundle.main.path(forResource: "出師表", ofType: "txt") {
        let content = try String(contentsOfFile: path)
        print(content)
    }
} catch {
    print(error.localizedDescription)
}
```

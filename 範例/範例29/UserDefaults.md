# UserDefaults

### 官方文件
[UserDefaults](https://developer.apple.com/documentation/foundation/userdefaults)

### 取得預設的 UserDefaults 實體

```Swift
let userDefaults = UserDefaults.standard
```

### 設置參數

```Swift
UserDefaults.standard.setValue(30, forKey: "Age")
```
### 取出

```Swift
let age = UserDefaults.standard.integer(forKey: "Age")
```

### 儲存不同資料型態

```Swift
UserDefaults.standard.setValue(30, forKey: "Age") 
UserDefaults.standard.setValue("Jerry", forKey: "Name") 
UserDefaults.standard.set(["QQ", "JJ"], forKey: "Array") 
UserDefaults.standard.set(["A": "A"], forKey: "Dictionary")
```

### 取得App路徑

```Swift
print(NSHomeDirectory())
```

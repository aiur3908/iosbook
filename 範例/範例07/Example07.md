# Chapter 7

### 學生類別

```Swift
class Student {
    var name: String
    var age: Int
    var studentID: String

    init(name: String, age: Int, studentID: String) {
        self.name = name
        self.age = age
        self.studentID = studentID
    }

    func sayHello() {
        print("Hello! 我是 \(name)")
    }
}

//初始化
let tom = Student(name: "Tom", age: 18, studentID: "A0001")

//存取實體屬性
print(tom.name) //Tom
print(tom.age) //18
print(tom.studentID) //A0001

//呼叫實體函式
tom.sayHello() //Hello! 我是 tom
```
### 擁有多個建構函式的學生類別

```Swift
class Student {

    var name: String

    init(name: String) {
        self.name = name
    }

    init() {
        self.name = ""
    }
}
```

### 宣告變數時就已經賦值的類別

```Swift
class Student {

    var name = "Tom"
    var age = 10
    var studentID = "A0001"
}
```

### 建構函式擁有預設值

```Swift
class Student {

    var name: String
    var age: Int
    var studentID: String
    
    init(name: String, age: Int = 18, studentID: String) {
        self.name = name
        self.age = age
        self.studentID = studentID
    }
    
}
```

### 學生結構

```Swift
struct Student {

    var name: String
    var age: Int
    var studentID: String

    func sayHello() {
        print("Hello! 我是 \(name)")
    }
}

//初始化
let tom = Student(name: "Tom", age: 18, studentID: "A0001")

//存取實體屬性
print(tom.name) //Tom
print(tom.age) //18
print(tom.studentID) //A0001

//呼叫實體函式
tom.sayHello() //Hello! 我是 tom
```

### 貓與狗類別

```Swift
class Cat {
    var age: Int
    var name: String

    init(age: Int, name: String) {
        self.age = age
        self.name = name
    }

    func voice() {
        print("喵!")
    }
}

class Dog {
    var age: Int
    var name: String

    init(age: Int, name: String) {
        self.age = age
        self.name = name
    }

    func voice() {
        print("汪!")
    }
}
```
### 將貓與狗類別抽象化成動物類別，並且繼承它

```Swift
class Animal {
    var age: Int
    var name: String
    
    init(age: Int, name: String) {
        self.age = age
        self.name = name
    }
    
    func voice() {
        
    }
}

class Cat: Animal {
    override func voice() {
        super.voice()
        print("喵!")
    }
}

class Dog: Animal {
    override func voice() {
        super.voice()
        print("汪!")
    }
}
```

### 額外增加一個豬類別，並且繼承動物類別

```Swift
class Pig: Animal {
    override func voice() {
        super.voice()
        print("噗")
    }
}
```

### 父類別可以當子類別的通用型別

```Swift
let animal: Animal = Dog(age: 15, name: " 阿 Q")
```

### 透過as來轉型

```Swift
//於狗類別增加一個玩飛盤的函式
class Dog: Animal {
    override func voice() {
        super.voice()
        print("汪!")
    }
    
    func playFrisbee() {
        print("玩飛盤")
    }
}

//父類別可以當子類別的通用型別
let someAnimal: Animal = Dog(age: 4, name: "阿狗")

//這個動物是一隻狗嗎 ?
if let dog = someAnimal as? Dog {
    //沒錯，他是一隻狗，試著叫牠玩飛盤
    dog.playFrisbee()
}
```

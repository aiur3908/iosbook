# 客製化View

### 範例01
基本範例

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter23/CustomView/CustomViewExample01)

程式碼

MyView.swift

```Swift
class MyView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .red
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        backgroundColor = .red
    }

}
```

ViewController.swift

```Swift
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let rect = CGRect(x: 100,
                          y: 100,
                          width: 100,
                          height: 100)
        let myView = MyView(frame: rect)
        view.addSubview(myView)
    }

}
```

### 範例02
增加UI元件

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter23/CustomView/CustomViewExample02)

程式碼

MyView.swift

```Swift
class MyView: UIView {

    var label: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLabel()
        backgroundColor = .red
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupLabel()
        backgroundColor = .red
    }
    
    private func setupLabel() {
        label = UILabel(frame: .zero)
        addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }

}
```

ViewController.swift

```Swift
class ViewController: UIViewController {
    
    @IBOutlet var myView: MyView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myView.label.text = "Hello World!"
    }

}
```

### 範例03
利用XIB來製作客製化View

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter23/CustomView/CustomViewExample03)

 程式碼
 
 MyView.swift
 
 ```Swift
 class MyView: UIView {

     @IBOutlet var label: UILabel!

     override init(frame: CGRect) {
         super.init(frame: frame)
         loadXibView()
     }
     
     required init?(coder: NSCoder) {
         super.init(coder: coder)
         loadXibView()
     }
     
     private func loadXibView() {
         // 取得當前的 Bundle
         let bundle = Bundle(for: type(of: self))
         // 透過當前的 Bundle 與 xib 名稱取得 xib 實體
         let nib = UINib(nibName: "MyView", bundle: bundle) 
         // 取得 xib 內的第一個畫面
         guard let xibView = nib.instantiate(withOwner: self,
                                             options: nil).first as? UIView else {
             return
         }
         addSubview(xibView)
         xibView.translatesAutoresizingMaskIntoConstraints = false
         xibView.topAnchor.constraint(equalTo: topAnchor).isActive = true
         xibView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
         xibView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
         xibView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
     }
     
 }
 ```
 
 ViewController.swift
 
 ```Swift
 class ViewController: UIViewController {
     @IBOutlet var myView: MyView!
     
     override func viewDidLoad() {
         super.viewDidLoad()
         myView.label.text = "Hello World!"
     }

 }
 ```

### 範例04
客製化UIControl

[範例04](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter23/CustomView/CustomViewExample04)

程式碼

MyControl.swift

```Swift
class MyControl: UIControl {

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.blue
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        backgroundColor = UIColor.blue
    }
}
```

ViewController.swift

```Swift
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let rect = CGRect(x: 100,
                          y: 100,
                          width: 100,
                          height: 100)
        let myControl = MyControl(frame: rect)
        view.addSubview(myControl)
        myControl.addTarget(self,
                            action: #selector(myControlTapped),
                            for: .touchUpInside)
    }
    
    @objc func myControlTapped() {
        print("Hello!")
    }

}
```

### 範例05
IBDesignable 與 IBInspectable

[範例05](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter23/CustomView/CustomViewExample05)

程式碼

MyView.swift

```Swift
@IBDesignable
class MyView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

}
```

### 範例06
客製化UIView + DataSource 與 Delegate

[範例06](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter23/CustomView/CustomViewExample06)

程式碼

MyView.swift

```Swift
protocol MyViewDataSource: AnyObject {
    func colorForLeftView(in myView: MyView) -> UIColor
    func colorForRightView(in myView: MyView) -> UIColor
}

@objc protocol MyViewDelegate: AnyObject {
    @objc optional func didTappedLeftView(in myView: MyView)
    @objc optional func didTappedRightView(in myView: MyView)
}

class MyView: UIView {
    @IBOutlet var leftView: UIView!
    @IBOutlet var rightView: UIView!
    
    weak var delegate: MyViewDelegate?
    weak var dataSource: MyViewDataSource?

    override init(frame: CGRect) {
        super.init(frame: frame)
        loadXibView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadXibView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        leftView.backgroundColor = dataSource?.colorForLeftView(in: self)
        rightView.backgroundColor = dataSource?.colorForRightView(in: self)
    }
    
    private func loadXibView() {
        // 取得當前的 Bundle
        let bundle = Bundle(for: type(of: self))
        // 透過當前的 Bundle 與 xib 名稱取得 xib 實體
        let nib = UINib(nibName: "MyView", bundle: bundle)
        // 取得 xib 內的第一個畫面
        guard let xibView = nib.instantiate(withOwner: self,
                                            options: nil).first as? UIView else {
            return
        }
        addSubview(xibView)
        xibView.translatesAutoresizingMaskIntoConstraints = false
        xibView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        xibView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        xibView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        xibView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    
    @IBAction func leftButtonTapped(_ sender: Any) {
        delegate?.didTappedLeftView?(in: self)
    }
    
    @IBAction func rightButtonTapped(_ sender: Any) {
        delegate?.didTappedRightView?(in: self)
    }
    
}
```

ViewController.swift

```Swift
class ViewController: UIViewController {
    @IBOutlet var myView: MyView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myView.dataSource = self
        myView.delegate = self
    }

}

extension ViewController: MyViewDataSource {
    func colorForLeftView(in myView: MyView) -> UIColor {
        return .red
    }
    
    func colorForRightView(in myView: MyView) -> UIColor {
        return .green
    }
}

extension ViewController: MyViewDelegate {
    func didTappedLeftView(in myView: MyView) {
        print("左邊的View被點擊")
    }
    
    func didTappedRightView(in myView: MyView) {
        print("右邊的View被點擊")
    }
}

```

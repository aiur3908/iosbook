# CALayer

### 官方文件
[CALayer](https://developer.apple.com/documentation/quartzcore/calayer)

### 圓角

```Swift
myView.layer.cornerRadius = 8
```

```Swift
myView.layer.cornerRadius = myView.frame.height / 2.0
```

### 邊框

```Swift
myView.layer.borderWidth = 1 
myView.layer.borderColor = UIColor.black.cgColor
```

### 陰影

```Swift
myView.layer.shadowOffset = CGSize(width: 3, height: 3)
myView.layer.shadowColor = UIColor.black.cgColor
myView.layer.shadowOpacity = 0.5 
myView.layer.shadowRadius = 3
```

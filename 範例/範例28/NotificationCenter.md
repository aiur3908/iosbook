# NotificationCenter

### 官方文件
[NotificationCenter](https://developer.apple.com/documentation/foundation/notificationcenter)

### 成為觀察者

```Swift
NotificationCenter.default.addObserver(
    self,
    selector: #selector(myFunc),
    name: NSNotification.Name("QQ"),
    object: nil)
    
   
@objc private func myFunc() {
    print("Hello")    
}
```

### 發送通知

```Swift
NotificationCenter.default.post(
    name: NSNotification.Name(rawValue: "QQ"),
    object: nil)
```

### 透過extension來建置通知名稱

```Swift
extension Notification.Name {
    static let userLogin = Notification.Name("com.dolphin.todolist.userlogin")
}
```

使用

```Swift
NotificationCenter.default.post( name: .userLogin, object: nil)
```


### 移除觀察者

```Swift
NotificationCenter.default.removeObserver(self)
```

### 發送參數給觀察者

```Swift
let userInfo: [String: Any] = ["name": "Jerry", "age": 30] 
NotificationCenter.default.post(name: .userLogin, object: nil, userInfo: userInfo)
```

接收參數

```Swift
@objc func myFunc(notification: Notification) { 
    if let userInfo = notification.userInfo,
       let name = userInfo["name"] as? String, 
        let age = userInfo["age"] as? Int {
        print("name = \(name)")
        print("age = \(age)")
    }
}
```

# GCD

### 官方文件
[GCD](https://developer.apple.com/documentation/DISPATCH)

### 同步

```Swift
let queue = DispatchQueue(label: "MyQueue") 
queue.sync {
    for _ in 1...3 { 
        print("A")
    }
}

for _ in 1...3 { 
    print("B")
}
```

### 非同步

```Swift
let queue = DispatchQueue(label: "MyQueue") 
queue.async {
    for _ in 1...3 {
        print("A")
    }
}

for _ in 1...3 { 
    print("B")
}
```

### Serial

```Swift
let queue = DispatchQueue(label: "MyQueue")
queue.async {
   for _ in 1...10 {
       print("A")
    }
}

queue.async {
    for _ in 1...10 { 
        print("B")
    } 
}

queue.async {
    for _ in 1...10 {
        print("C")
    }
}
```

### Concurrent

```Swift
let queue = DispatchQueue(label: "MyQueue", attributes: .concurrent)
queue.async {
    for _ in 1...10 {
        print("A") 
    }
}

queue.async {
    for _ in 1...10 {
        print("B") 
    }
}

queue.async {
    for _ in 1...10 {
        print("C")
    }
}
```

### DispatchGroup

```Swift
let group = DispatchGroup()
let queue1 = DispatchQueue(label: "MyQueue") 

queue1.async(group: group) {
    print("執行任務1") 
    sleep(5)
    print("任務1完成")
}

let queue2 = DispatchQueue(label: "MyQueue")
queue2.async(group: group) {
    print("執行任務2")
    sleep(3)
    print(" 任務 2 完成 ")
}
    
group.notify(queue: .main) { 
    print("任務都執行完成了")
} 
```

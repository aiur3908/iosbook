# Timer

### 官方文件
[Timer](https://developer.apple.com/documentation/foundation/timer)

### 範例01
基本使用方式

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter30/Timer/TimerExample01)

程式碼

```Swift
class ViewController: UIViewController {

    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timer = Timer.scheduledTimer(timeInterval: 1,
                                     target: self,
                                     selector: #selector(sayHello(timer:)),
                                     userInfo: nil,
                                     repeats: true)
    }

    @objc func sayHello(timer: Timer) {
        print("Hello")
    }

}
```


### 停止Timer

```Swift
timer?.invalidate()
```

### 透過UserInfo傳遞資料

```Swift
class ViewController: UIViewController {

    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let userInfo: [String: Any] = ["name": "Jerry", "age": 30]
        timer = Timer.scheduledTimer(
            timeInterval: 1,
            target: self,
            selector: #selector(sayHello(timer:)),
            userInfo: userInfo,
            repeats: true)
    }

    @objc func sayHello(timer: Timer) {
        if let userInfo = timer.userInfo as? [String: Any],
           let name = userInfo["name"] {
            print(name)
        }
    }

}
```

### 範例02
倒數計時範例

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter30/Timer/TimerExample02)

程式碼

```Swift
class ViewController: UIViewController {
    @IBOutlet var infoLabel: UILabel!
    
    var seconds = 10
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        timer = Timer.scheduledTimer(
            timeInterval: 1,
            target: self,
            selector: #selector(countdown(timer:)),
            userInfo: nil,
            repeats: true)
    }
    
    @objc func countdown(timer: Timer) {
        seconds -= 1
        if seconds == 0 {
            // 時間到
            infoLabel.text = "新年快樂"
            timer.invalidate()
        } else {
            infoLabel.text = "\(seconds)"
        }
    }
}
```

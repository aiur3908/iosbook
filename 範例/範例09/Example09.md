# Chapter 9

### UIViewController 生命週期

![image](Example09001.png)


### 畫面載入時的會觸發的程式碼

```Swift
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
    }
    
    override func viewWillAppear(_ animated: Bool) { 
        super.viewWillAppear(animated)
        print("viewWillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) { 
        super.viewDidAppear(animated)
        print("viewDidAppear")
    }

}
```

### 畫面結束時會觸發的程式碼

```Swift
import UIKit

class ViewController: UIViewController {

    override func viewWillDisappear(_ animated: Bool) { 
        super.viewWillDisappear(animated)
        print("viewWillDisappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("viewDidDisappear")
    }

}
```

### 範例01 
IBoutlet範例

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter09/Example0901)

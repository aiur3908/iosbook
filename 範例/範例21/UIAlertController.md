# UIAlertController

### 官方文件

[UIAlertController](https://developer.apple.com/documentation/uikit/uialertcontroller)

### 範例01
基本語法

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter21/UIAlertController/UIAlertControllerExample01)

程式碼

```Swift
let alertController = UIAlertController(title: "請選擇", message: "你喜歡貓還是狗?", preferredStyle: .alert)

let catAction = UIAlertAction(title: "貓", style: .default, handler: { _ in
    print("你喜歡貓!")
})

let dogAction = UIAlertAction(title: "狗", style: .default, handler: { _ in 
    print("你喜歡狗")
})

alertController.addAction(catAction) 
alertController.addAction(dogAction) 
present(alertController, animated: true, completion: nil)
```
### 範例02
增加輸入框到UIAlertController之中

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter21/UIAlertController/UIAlertControllerExample02)

程式碼

```Swift
let alertController = UIAlertController(title: "提示", message: "請輸入帳號密碼", preferredStyle: .alert)
alertController.addTextField(configurationHandler: { textField in
    textField.placeholder = "帳號"

})

alertController.addTextField(configurationHandler: { textField in
    textField.placeholder = "密碼"
    textField.isSecureTextEntry = true
})

let loginAction = UIAlertAction(title: " 登入 ", style: .default, handler: { _ in
    if let textFields = alertController.textFields,
       let account = textFields[0].text,
       let password = textFields[1].text {
        print("帳號:\(account), 密碼: \(password)")
    }
})

let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
alertController.addAction(loginAction)
alertController.addAction(cancelAction)
present(alertController, animated: true, completion: nil)
```

### 範例03
動作表

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter21/UIAlertController/UIAlertControllerExample03)

程式碼

```Swift
let alertController = UIAlertController(title: "請選擇", message: "最喜歡哪種動物?", preferredStyle: .actionSheet)
let catAction = UIAlertAction(title: "貓", style: .default, handler: nil)
let dogAction = UIAlertAction(title: "狗", style: .default, handler: nil)
let dolphinAction = UIAlertAction(title: "海豚", style: .default, handler: nil)
alertController.addAction(catAction)
alertController.addAction(dogAction)
alertController.addAction(dolphinAction)


let cancelAction = UIAlertAction(title: "都不喜歡 :(", style: .cancel, handler: nil)
alertController.addAction(cancelAction)
present(alertController, animated: true, completion: nil)
```

### 增加一個專門顯示訊息的函式

```Swift
func showAlerController(title: String, message: String) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle:
.alert)
    let confirmAction = UIAlertAction(title: "確認", style: .default, handler: nil) alertController.addAction(confirmAction)
    present(alertController, animated: true, completion: nil)
}


// 當使用者帳號或密碼錯誤時
showAlerController(title: "錯誤", message: "帳號或密碼輸入錯誤")
// 當網路連線錯誤時
showAlerController(title: "錯誤", message: "網路錯誤，請確認你的網路狀態")
```

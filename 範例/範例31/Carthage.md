# Carthage

### 官方網站
[Carthage](https://github.com/Carthage/Carthage)

### 安裝

先安裝Homebrew

[Homebrew](https://brew.sh/)

安裝指令

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install. sh)"
```

安裝 Carthage

```
brew install carthage
```

確認版本

```
carthage version
```

### IQKeyboardManager

新增Cartfile並增加以下內容

```
github "hackiftekhar/IQKeyboardManager"
```

於終端機中輸入

```
carthage update
```

專案中的Shell

```
/usr/local/bin/carthage copy-frameworks
```
Input Files

```
$(SRCROOT)/Carthage/Build/iOS/IQKeyboardManagerSwift.framework
```

Output Files

```
$(BUILT_PRODUCTS_DIR)/$(FRAMEWORKS_FOLDER_PATH)/IQKeyboardManagerSwift.framework
```

# Cocoapods

### 官方網站
[Cocoapods](https://cocoapods.org/)

### 安裝

```
sudo gem install cocoapods
```

### IQKeyboardManager

[IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager)

修改PodFile，並且增加以下的內容

```
pod 'IQKeyboardManagerSwift'
```

安裝指令

```
pod install
```

使用方式

AppDelegate.swift

```Swift
import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, 
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // 開啟 IQKeyboardManager 
        IQKeyboardManager.shared.enable = true return true
    } 
}
```

點空白處收合鍵盤

```Swift
IQKeyboardManager.shared.shouldResignOnTouchOutside = true
```

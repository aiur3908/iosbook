# Chapter 3

### 宣告陣列

```Swift
var score: [Int] = [80, 88, 90]
```

### 透過型別推斷來宣告陣列

```Swift
var score = [80, 88, 90]
```

### 宣告空陣列

```Swift
var score: [Int] = []
```
### 宣告陣列的另一種方式

```Swift
var score: Array<Int> = [88, 80]
```

### 取出陣列的值

```Swift
var score = [80, 88, 90] 
print(score[0]) //80 
print(score[1]) //88
print(score[2]) //90
```
### 陣列的操作 - 新增

```Swift
var score = [80, 88, 90] 
score.append(100)
print(score[3]) // 100
```
### 陣列的操作 - 刪除

```Swift
var score = [80, 88, 90]
score.remove(at: 0) // score = [88, 90]
print(score[0]) // 88
```
### 陣列的操作 - 修改

```Swift
var score = [80, 88, 90]
score[0] = 100
print(score[0]) //100
```

### 陣列的操作 - 合併

```Swift
let score1 = [80, 88, 90]
let score2 = [100, 120]
let score = score1 + score2 // [80, 88, 90, 100, 120]
print(score[3]) // 100
```

### 陣列的操作 - 排序

```Swift
let array = [6,2,4,1]
let sortedArray1 = array.sorted(by: >) 
let sortedArray2 = array.sorted(by: <)
print(sortedArray1) // [6, 4, 2, 1]
print(sortedArray2) // [1, 2, 4, 6]
```

### 多維陣列

```Swift
var array: [[Int]] = [[0], [0,1]] 
print(array[0]) // [0] 
print(array[0][0]) // 0
```
### 宣告字典

```Swift
var dictionary: [String: Int] = ["A": 10, "B": 15, "C": 20]
```

### 透過型別推斷來宣告字典

```Swift
var dictionary = ["A": 10, "B": 15, "C": 20]
```
### 字典的操作 - 新增與修改

```Swift
var dictionary = ["A": 10, "B": 15, "C": 20] 
/// 新增
dictionary["D"] = 30
/// 修改
dictionary["A"] = 20
```
### 字典的操作 - 刪除

```Swift
var dictionary = ["A": 10, "B": 15, "C": 20] 
dictionary.removeValue(forKey: "A")
```
### 獲取字典內所有的Key

```Swift
var dictionary = ["A": 10, "B": 15, "C": 20] 
let keys = dictionary.keys
```

### 獲取字典內所有的Value

```Swift
var dictionary = ["A": 10, "B": 15, "C": 20] 
let values = dictionary.values
```

### 宣告一個整數集合

```Swift
let numbersSet: Set<Int> = [1, 2 ,3]
```
### 取得集合內的值

```Swift
for number in numbersSet {
    print(number)
}
//1 
//3 
//2
```
### 集合的操作 - 新增

```Swift
var numbersSet: Set<Int> = [1, 2 ,3]
numbersSet.insert(4) 
print(numbersSet) // [1, 2, 3, 4]
```

### 集合的操作 - 刪除

```Swift
var numbersSet: Set<Int> = [1, 2 ,3] 
numbersSet.remove(1) 
print(numbersSet) // [2, 3]
```

### 集合與陣列的轉換

```Swift
var numberArray = [1, 2, 3, 3] 
let numberSet = Set(numberArray) 
numberArray = Array(numberSet) 
print(numberArray) //[1, 2, 3]
```

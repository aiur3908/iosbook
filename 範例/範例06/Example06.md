# Chapter 6

### 透過條件判斷式來強制解包

```Swift
let a = "10"
let b = Int(a)
if b != nil {
    print(b!) //10
}
```

### 可選綁定

```Swift
let optionalInt: Int? = 10

if let intValue = optionalInt {
    //當可選型別有值時
    print("optionalInt 的值是 \(intValue)")
} else {
    //當可選型別為 nil 時
    print("optionalInt 是 nil")
}
```
### 多重可選綁定

```Swift
let optionalInt: Int? = 10
let optionalDouble: Double? = 0.05
let optionalString: String? = nil


if let intValue = optionalInt,
   let doubleValue = optionalString,
   let stringValue = optionalString {
    // 當可選型別均有值時
} else {
    //當可選型別任意一個為 nil 時
}
```
### Guard 語法

```Swift
func interview(age: Int, toeic: Int) {
    guard age >= 18 else {
        print("年紀不夠")
        return // 提前退出
    }
    
    guard toeic >= 750 else {
        print("沒過英文門檻")
        return // 提前退出
    }
    
    print("開始面試")
}

interview(age: 16, toeic: 750) // 年紀不夠
interview(age: 20, toeic: 740) // 沒過英文門檻
interview(age: 20, toeic: 750) // 開始面試
```

### 透過Guard進行可選綁定

```Swift
func myFunc() {
    let optionalInt: Int? = 10
    guard let intValue = optionalInt else {
        return
    }
    print(intValue)
}
```

### 透過FatalError來提前退出

```Swift
func myFunc(string: String) -> Int {
    guard let intValue = Int(string) else {
        fatalError("String 轉換 Int 失敗")
    }
    return intValue
}

myFunc(string: "10")
```

### 透過兩個問號解包

```Swift
let optionalInt: Int? = nil
let intValue = optionalInt ?? 15
print(intValue) //15
```

### 隱式解包可選型別

```Swift
let a: Int! = 1
print(a + 1)
```

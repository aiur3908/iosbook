# Error

### 官方文件

[Error](https://developer.apple.com/documentation/swift/error)

### 設計一個有關登入的Error

```Swift
enum LoginError: Error { 
    // 密碼錯誤
    case wrongPassword 
    // 帳號不存在
    case accountNotExist
}
```

```Swift
func login(account: String, password: String) throws { 
    if account != "Jerry001" {
        throw LoginError.accountNotExist
    }
    
    if password != "abcd1234" {
        throw LoginError.wrongPassword
    }
    
    print("登入成功")
}
```

使用do catch來捕捉錯誤

```Swift
do {
    try login(account: "Jerry002", password: "QQ")
} catch { 
    print(error)
}
```
捕捉各種錯誤

```Swift
do {
    try login(account: "Jerry002", password: "QQ")
} catch LoginError.accountNotExist { 
    print("帳號不存在")
} catch LoginError.wrongPassword { 
    print("密碼錯誤")
} catch {
    print("其他錯誤")
}
```

將錯誤往外拋出

```Swift
func myFunc() throws {
    try login(account: "Jerry001", password: "ABCD1234")
}
```

### 將錯誤轉換成可選值

```Swift
func someThrowingFunction() throws -> Int { 
    // ...
}
```

```Swift
let value = try? someThrowingFunction()
```

### 忽略錯誤

```Swift
let value = try! someThrowingFunction()
```

### LocalizedError

```Swift
enum LoginError: Error { 
    // 密碼錯誤
    case wrongPassword 
    // 帳號不存在
    case accountNotExist
}

extension LoginError: LocalizedError { 
    var errorDescription: String? {
        switch self {
        case .wrongPassword:
            return "密碼錯誤" 
        case .accountNotExist:
            return "密碼不存在"
        }
    } 
}
```

實際使用

```Swift
do {
    try login(account: "Jerry001", password: "QQ")
} catch {
    // 密碼錯誤
    print(error.localizedDescription)
}
```

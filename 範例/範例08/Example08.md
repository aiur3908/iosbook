# Chapter 8


### 範例01 
透過UILabel於畫面中顯示Hello World

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter08/Example0801)

### 透過viewDidLoad印製出Hello World

```Swift
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("Hello World")
    }

}
```
### 透過程式碼修改ViewController的背景顏色

```Swift
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.red
    }


}
```

### 範例02 
產生多個頁面，並且切換頁面

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter08/Example0804)

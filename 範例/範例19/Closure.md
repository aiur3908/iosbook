# Closure

### 無傳入值、無回傳值的函式與 Closure

函式

```Swift
func myFunc() { 
    print("Hello")
}

myFunc()
```

Closure

```Swift
var closure: () -> Void

closure = { 
    print("Hello")
}

closure()
```

### 有傳入值、無回傳值的函式與 Closure

函式

```Swift
func myFunc(name: String) { 
    print("Hello \(name)")
}

myFunc(name: "Jerry")
```

Closure

```Swift
var closure: (String) -> Void

closure = { name in 
    print("Hello \(name)")
} 

closure("Jerry")
```
省略參數

```Swift
closure = { _ in 
    print("Hello")
}
```

### 有多個傳入值、無回傳值的函式與 Closure

函式

```Swift
func myFunc(number1: Int, number2: Int) { 
    print("\(number1 + number2)")
}

myFunc(number1: 1, number2: 1)
```
Closure

```Swift
var closure: (Int, Int) -> Void

closure = { number1, number2 in 
    print("\(number1 + number2)")
} 

closure(1,1)
```

### 無傳入值、有回傳值的函式與 Closure

函式

```Swift
func myFunc() -> String { 
    return "Hello"
}

let message = myFunc()
```

Closure

```Swift
var closure: () -> String

closure = {
    return "Hello"
}

let message = closure()
```

### 有傳入值、有回傳值的函式與 Closure

函式

```Swift
func myFunc(name: String) -> String { 
    return "Hello \(name)"
}

let message = myFunc(name: "Jerry")
```

Closure

```Swift
var closure: (String) -> String

closure = { name in
    return "Hello \(name)"
}

let message = closure("Jerry")
```

### 傳入值可以使用 $0~$n 來代替

省略前

```Swift
var closure: (String) -> String

closure = { name in
    return "Hello \(name)"
}

let message = closure("Jerry")
```

省略後

```Swift
var closure: (String) -> String

closure = {
    return "Hello \($0)"
}

let message = closure("Jerry")
```
### 單行可省略 return 不寫

省略前

```Swift
var closure: (String) -> String

closure = { name in
    return "Hello \(name)"
}

let message = closure("Jerry")
```

省略後

```Swift
closure = { name in 
    "Hello \(name)"
}
```

### 簡化程式碼

```Swift
var closure: (String) -> String

closure = { "Hello \($0)" }

let message = closure("Jerry")
```

### 把 Closure 當參數傳遞

```Swift

func someFunctionThatTakesAClosure(closure: () -> Void) { 
    //執行了某些程式碼後

    //執行 clsoure
    closure() 
}

// 呼叫函式，並且定義對應的 Closure 區塊
someFunctionThatTakesAClosure(closure: {
    print("Hello") 
})
```

### 單一函式中，擁有多個 Closure 的參數

```Swift

func myFunc(age: Int, onSuccess: () -> Void, onError: () -> Void) { 
    if age >= 18 {
        onSuccess() 
    } else {
        onError() 
    }
}

// 呼叫函式
myFunc(age: 17, onSuccess: {
    print("成功") 
}, onError: {
    print("失敗") 
})
```

### 尾隨閉包簡寫

```Swift
someFunctionThatTakesAClosure { 
    print("Hello")
}
```

### 範例01
實際應用

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter19/Closure/ClosureExample01)

程式碼

ViewController.swift

```Swift
class ViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    var tableViewData: [Bool] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    private func setupTableView() {
        for _ in 0...15 {
            tableViewData.append(true)
        }
        
        let cellNib = UINib(nibName: "MyTableViewCell", bundle: .main)
        tableView.register(cellNib, forCellReuseIdentifier: "Cell")
        tableView.dataSource = self
    }

}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? MyTableViewCell else {
            fatalError()
        }

        cell.mySwitch.isOn = tableViewData[indexPath.row]
        cell.switchChangedHandler = { [unowned self] isOn in
            self.tableViewData[indexPath.row] = isOn
        }
        return cell
    }
}
```

MyTableViewCell.swift

```Swift
class MyTableViewCell: UITableViewCell {

    @IBOutlet var mySwitch: UISwitch!
    var switchChangedHandler: ((Bool) -> Void)?

    @IBAction func switchDidValueChanged(_ sender: UISwitch) {
        switchChangedHandler?(sender.isOn)
    }
    
}
```

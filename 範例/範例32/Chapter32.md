# Chapter 32

### 蘋果開發者計畫
[蘋果開發者計畫](https://developer.apple.com/programs/)

### 蘋果開發者網站
[蘋果開發者網站](https://developer.apple.com/)

### App Store Connect
[App Store Connect](https://appstoreconnect.apple.com/)

### 查詢iOS版本市佔率
[市佔率](https://developer.apple.com/support/app-store/)

### 免費icon下載
[免費icon](https://www.flaticon.com/)

### 製作各種大小的icon
[makeappicon](https://makeappicon.com/)

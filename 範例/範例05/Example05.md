# Chapter 5

### 函式範例

```Swift
func sayHello(name: String) -> String {
    let helloMessage = "Hello, " + name + "!"
    return helloMessage
}

let message = sayHello(name: "Jerry")
print(message) //Hello, Jerry!
```

### 無傳入值、無回傳值的函式

```Swift
func greet() {
    print("Hello")
}

// 呼叫函式
greet() //Hello
```

### 無傳入值、有回傳值的函式

```Swift
func greet() -> String {
    return "Hello"
}

//呼叫函式
let message = greet()
print(message) //Hello
```

### 有傳入值、無回傳值的函式

```Swift
func greet(person: String) {
    print("Hello \(person)")
}

// 呼叫函式
greet(person: "Jerry") //Hello Jerry
```

### 有傳入值、有回傳值的函式

```Swift
func greet(person: String) -> String {
    return "Hello \(person)"
}

// 呼叫函式
let message = greet(person: "Jerry")
print(message) //Hello Jerry
```
### 多個傳入值的函式

```Swift
func myFunc(name: String, age: Int) {
    print(name + " 今年 \(age) 歲 ")
}

/// 呼叫函式
myFunc(name: "Jerry", age: 30) //Jerry今年 30 歲
```

### 多重回傳值函式

```Swift
func myFunc() -> (Int, Int) {
    return (10, 20)
}
```
### 隱式返回函式

```Swift
func greet(person: String) -> String {
    "你好呀\(person)"
}

print(greet(person: "Jerry")) // 你好呀Jerry
```

### 省略參數名稱

```Swift
func myFunc(_ name: String, _ age: Int) {
    print(name + "今年\(age)歲")
}

myFunc("Jerry", 18) //Jerry今年18歲
```

### 參數外部名稱

```Swift
func greet(person: String, from hometown: String) -> String {
    return "Hello \(person)! 很高興你可以從\(hometown)到此地遊玩"
}

greet(person: "Jerry", from: "桃園")
```

### 有預設值的參數

```Swift
func myFunc(int1: Int, int2: Int = 10) {
    print(int1 + int2)
}

myFunc(int1: 10) //20
myFunc(int1: 10, int2: 20) //30
```

### 可變參數

```Swift
func myFunc(numbers: Int...) {
    var total = 0
    for number in numbers {
        total += number
    }
    print("總計\(total)")
}

myFunc(numbers: 1) // 總計 1
myFunc(numbers: 1,2,3) // 總計 6
```

### 輸入輸出參數

```Swift
func myFunc(a: inout Int) {
    a = a+5
}
var myInt = 3
myFunc(a: &myInt)
print(myInt) //8
```

### 相同名稱的函式

```Swift
func greet() {
    print("Hello")
}

func greet(name: String) {
    print("哈囉\(name)")
}

func greet(name: String) -> String {
    return "哈囉\(name)"
}
```

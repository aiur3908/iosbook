# Chapter 2

### 常數

```Swift
let pi = 3.14
```
### 變數

```Swift
var age = 15
age = 16
```
### 常數與變數的命名

```Swift
let π = 3.14159
var 你好 = "你好世界"
let 🐬 = "海豚"
```
### 使用系統關鍵字當名稱

```Swift
let `let` = 123
var `var` = 123
`var` = 456
```
### 字串串接

```Swift
let firstName = "Jerry"
let lastName = "You"
let fullName = firstName + lastName 
print(fullName) //JerryYou
```
### 字串中包含值

```Swift
let apples = 3
let banana = 5
let appleSummary = " 我有 \(apples) 個蘋果 "
let fruitSummary = " 我有 \(apples + banana) 個水果 "
print(appleSummary) // 我有 3 個蘋果 
print(fruitSummary) // 我有 8 個水果
```

### 將字串轉換成大寫

```Swift
let hello = "Hello, World!"
let newHello = hello.uppercased()
```

### 將字串轉換成小寫

```Swift
let hello = "Hello, World!"
let newHello = hello.lowercased()
```

### 取得字串長度

```Swift
let hello = "Hello, World!"
print(hello.count) //13
```
### 多行文字

```Swift
let hello = """ 
Hello,
  World!!
    你好，
      世界 !!
"""
```

### 將整數轉換成浮點數

```Swift
let intValue = 3
let doubleValue = Double(intValue)
```

### 將整數轉換成字串

```Swift
let intValue = 3
let stringValue = String(intValue)
```

### 將字串轉換成數字，轉換失敗

```Swift
let stringValue = "我不是數字"
let intValue = Int(stringValue)
```
### 將字串轉換成數字，轉換成功並且解包

```Swift
let stringValue = "1"
let intValue = Int(stringValue)!
```
### 型別別名

```Swift
/// 定義一個型別別名，資料型態為 Int 
typealias Age = Int
/// 使用型別別名來取代原本的資料型態名稱 
var age: Age = 10
```

### 宣告元祖

```Swift
let http404Error = (404, "NotFound")
let statusCode = http404Error.0 // 404
let statusMessage = http404Error.1 // NotFound
```
### 將元組命名

```Swfit
let http404Error = (statusCode: 404, statusMessage: "NotFound") print(http404Error.statusCode) //404 
print(http404Error.statusMessage) //NotFound
```
### 將元組內容取出，並分解成常數

```Swift
let http404Error = (statusCode: 404, statusMessage: "NotFound")
let (statusCode, statusMessage) = http404Error 
print(statusCode) // 404
print(statusMessage) //NotFound
```
### 忽略某個元組內的值

```Swift
let http404Error = (statusCode: 404, statusMessage: "NotFound") 
let (statusCode, _) = http404Error
print(statusCode) // 404
```

### 整數亂數

```Swift
let randomIntValue = Int.random(in: 1...6)
print(randomIntValue)
```

### 浮點數亂數

```Swift
let randomDoubleValue = Double.random(in: 1.0...6.0)
print(randomDoubleValue)
```

### 實體屬性與函式

```Swift
let string = "Hello World"
let count = string.count
let newString = string.uppercased()
```

### 靜態屬性與函式

```Swift
let intMax = Int.max
let randomValue = Int.random(in: 1...5)
```

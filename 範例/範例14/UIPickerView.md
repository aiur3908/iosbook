# UIPickerView

### 官方文件

[UIPickerView](https://developer.apple.com/documentation/uikit/uipickerview)

### 範例01
基本使用方式

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter14/UIPickerView/UIPickerViewExample01)

程式碼

```Swift
class ViewController: UIViewController {

    @IBOutlet var pickerView: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView.dataSource = self
        pickerView.delegate = self
    }


}

extension ViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 10
    }
}

extension ViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row)"
    }
}
```

### 範例02
透過陣列設置

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter14/UIPickerView/UIPickerViewExample02)

程式碼

```Swift
class ViewController: UIViewController {

    @IBOutlet var pickerView: UIPickerView!
    var titles: [String] = ["Apple", "Avocado", "Banana", "Cherry", "Coconut", "Grape"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView.dataSource = self
        pickerView.delegate = self
    }


}

extension ViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return titles.count
    }
}

extension ViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return titles[row]
    }
}

```

### 得知使用者選擇

```Swift
let row = pickerView.selectedRow(inComponent: 0)

print(" 使用者目前選擇了 \(titles[row])")
```

### didSelectRow

UIPickerViewDelegate中可實作的函式，當使用者選擇時會觸發此函式

```Swift

extension ViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return titles[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(titles[row])
    }
}
```

### 範例03
建立多個Component的UIPickerView

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter14/UIPickerView/UIPickerViewExample03)

程式碼

```Swift
class ViewController: UIViewController {

    @IBOutlet var pickerView: UIPickerView!
    let items: [[String]] = [["Apple", "Banana", "Cherry"], ["蘋果", "香蕉", "櫻桃"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView.dataSource = self
        pickerView.delegate = self
    }


}

extension ViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return items.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return items[component].count
    }
}

extension ViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return items[component][row]
    }
}
```

# UITabbarController

### 官方文件

[UITabbarController](https://developer.apple.com/documentation/uikit/uitabbarcontroller)

### 範例01
基本使用方式

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter22/UITabbarController/UITabbarControllerExample01)

### 切換當前頁面

```Swift
tabBarController?.selectedIndex = 2
```

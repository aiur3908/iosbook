# UIPageViewController

### 官方文件

[UIPageViewController](https://developer.apple.com/documentation/uikit/uipageviewcontroller)

### 範例01
基本使用方式

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter22/UIPageViewController/UIPageViewControllerExample01)

程式碼

```Swift
class MyPageViewController: UIPageViewController {

    var pages: [UIViewController] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        let storyboard = UIStoryboard(name: "Main", bundle: .main)
        let page1 = storyboard.instantiateViewController(withIdentifier: "RedViewController")
        let page2 = storyboard.instantiateViewController(withIdentifier: "BlueViewController")
        let page3 = storyboard.instantiateViewController(withIdentifier: "GreenViewController")
        page1.view.tag = 0
        page2.view.tag = 1
        page3.view.tag = 2
        pages.append(page1)
        pages.append(page2)
        pages.append(page3)
        dataSource = self
        delegate = self
        setViewControllers([page1], direction: .forward, animated: true, completion: nil)
    }
}

extension MyPageViewController: UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {

        // 取得當前頁面
        let currendIndex = pages.firstIndex(of: viewController)!

        if currendIndex > 0 {
            // 如果當前頁面不是第一頁，那麼前一頁就為當前頁面的 idnex - 1
            return pages[currendIndex - 1]
        } else {
            // 如果當前頁面為第一頁，那麼前一頁就為 nil
            return nil
        }
    }
    

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {

        // 取得當前頁面
        let currendIndex = pages.firstIndex(of: viewController)!
        
        if currendIndex < pages.count - 1 {
            // 如果當前的頁面不是最後一頁，那麼下一頁為當前頁面的 index + 1
            return pages[currendIndex + 1]
        } else {
            // 如果當前頁面為最後一頁，那麼下一頁就為 nil
            return nil
        }
    }
}

  
extension MyPageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {
        let currentPage = pageViewController.viewControllers!.first!
        let index = pages.firstIndex(of: currentPage)!
        print("當前頁面的 Index 為 \(index)")
   }
}

```

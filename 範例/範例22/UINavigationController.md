# UINavigationController

### 官方文件

[UINavigationController](https://developer.apple.com/documentation/uikit/uinavigationcontroller)

### 範例01
基本使用方式

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter22/UINavigationController/UINavigationControllerExample01)

### 設置標題

```Swift
override func viewDidLoad() { 
    super.viewDidLoad()
    title = "第一頁"
}
```

或者使用此語法

```Swift
override func viewDidLoad() { 
    super.viewDidLoad() 
    navigationItem.title = "第一頁"
}
```

### 切換頁面

透過Segue

```Swift
performSegue(withIdentifier: "DisplayBVC", sender: nil)
```

透過push

```Swift
navigationController?.pushViewController(bVC, animated: true)
```

### 結束頁面

```Swift
navigationController?.popViewController(animated: true)
```

### 一口氣回到第一頁

```Swift
navigationController?.popToRootViewController(animated: true)
```

### 取得所有堆疊頁面

```Swfit
let viewControllers = navigationController?.viewControllers
```

### 替換堆疊頁面

```Swift
let viewControllers = navigationController!.viewControllers 
let newViewControllers = [viewControllers[0],
                          viewControllers[3]] 
                          
navigationController?.setViewControllers(newViewControllers, animated: true)
```

### 範例02
增加UIBarButton

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter22/UINavigationController/UINavigationControllerExample02)

### 透過程式碼增加UIBarButton

```Swift
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let leftButton = UIBarButtonItem(title: "左邊按鈕",
                                         style: .plain,
                                         target: self,
                                         action: #selector(leftButtonTapped))
        let rightButton = UIBarButtonItem(title: "右邊按鈕",
                                          style: .plain,
                                          target: self,
                                          action: #selector(rightButtonTapped))
        navigationItem.leftBarButtonItems = [leftButton]
        navigationItem.rightBarButtonItems = [rightButton]
    }

    
    @objc func leftButtonTapped() {
        print("左邊按鈕點擊")
    }

    @objc private func rightButtonTapped() {
        print("右邊按鈕點擊")
    }
    
}

```

### 範例03
UINavigationController + UITableView，擁有增加與移動的功能

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter22/UINavigationController/UINavigationControllerExample03)

程式碼
```Swift
class ViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    var tableViewData: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    private func setupTableView() {
        for index in 0...10 {
            tableViewData.append(String(index))
        }
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @IBAction func addButtonTapped(_ sender: Any) {
        tableView.setEditing(true, animated: true)
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        tableView.setEditing(false, animated: true)
    }
    
    
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        // 取出原本位置的資料
        let sourceData = tableViewData[sourceIndexPath.row]
        // 將原本位置的資料移除
        tableViewData.remove(at: sourceIndexPath.row)
        // 新增到新的位置
        tableViewData.insert(sourceData, at: destinationIndexPath.row)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") else {
            fatalError()
        }
        
        cell.textLabel?.text = tableViewData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .insert {
            tableViewData.insert(String(Int.random(in: 0...10)),
                                    at: indexPath.row + 1)
            let newIndexPath = IndexPath(row: indexPath.row + 1,
                                         section: indexPath.section)
            tableView.beginUpdates()
            tableView.insertRows(at: [newIndexPath], with: .fade)
            tableView.endUpdates()
        }
    }
}


extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .insert
    }
}
```

### 範例04
UINavigationController + UITableView，擁有刪除的功能

[範例04](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter22/UINavigationController/UINavigationControllerExample04)

```Swift
class ViewController: UIViewController {
    
    var tableViewData: [String] = []
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    private func setupTableView() {
        for index in 0...10 {
            tableViewData.append(String(index))
        }
        tableView.dataSource = self
        tableView.delegate = self
    }

    @IBAction func editButtonTapped(_ sender: Any) {
        tableView.setEditing(true, animated: true)
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        tableView.setEditing(false, animated: true)
    }
    
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") else {
            fatalError()
        }
        
        cell.textLabel?.text = tableViewData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //移除資料
            tableViewData.remove(at: indexPath.row)
            //移除Row
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}


extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
}

```


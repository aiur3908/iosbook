# Chapter 11

### 縮放係數

```Swift
let scale = UIScreen.main.scale
```

### 不同手機的縮放比例

[參考網站](https://www.paintcodeapp.com/news/ultimate-guide-to-iphone-resolutions)

### 是否自動轉換成條件約束

```Swift
myView.translatesAutoresizingMaskIntoConstraints = false
```

### 範例01 
垂直水平置中於畫面中央，且寬高均為 150

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter11/Example1101)

透過程式碼來定義

```Swift
let myView = UIView(frame: .zero)
myView.translatesAutoresizingMaskIntoConstraints = false
myView.backgroundColor = .green
view.addSubview(myView)

myView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
myView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
myView.widthAnchor.constraint(equalToConstant: 150).isActive = true
myView.heightAnchor.constraint(equalToConstant: 150).isActive = true
```

### 範例02 
寬高利用距離來定義

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter11/Example1102)

透過程式碼來定義

```Swift
let myView = UIView(frame: .zero)
myView.translatesAutoresizingMaskIntoConstraints = false
myView.backgroundColor = .green
view.addSubview(myView)

myView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
myView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
myView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
myView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = tru
```

### 範例03 
寬高使用比例來設定

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter11/Example1103)

透過程式碼來定義

```Swift
let myView = UIView(frame: .zero)
myView.translatesAutoresizingMaskIntoConstraints = false
myView.backgroundColor = .green
view.addSubview(myView)

myView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
myView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
myView.heightAnchor.constraint(equalToConstant: 150).isActive = true
myView.widthAnchor.constraint(equalTo: myView.heightAnchor, multiplier: 0.5).isActive = true
```

### 範例04 
與其他 View 做比較，定義寬高

[範例04](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter11/Example1104)

透過程式碼來定義

```Swift
let redView = UIView(frame: .zero)
redView.translatesAutoresizingMaskIntoConstraints = false
redView.backgroundColor = .red
view.addSubview(redView)
redView.widthAnchor.constraint(equalToConstant: 300).isActive = true
redView.heightAnchor.constraint(equalToConstant: 300).isActive = true
redView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
redView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true

let whiteView = UIView(frame: .zero)
whiteView.translatesAutoresizingMaskIntoConstraints = false
whiteView.backgroundColor = .white
redView.addSubview(whiteView)
whiteView.centerXAnchor.constraint(equalTo: redView.centerXAnchor).isActive = true
whiteView.centerYAnchor.constraint(equalTo: redView.centerYAnchor).isActive = true
whiteView.widthAnchor.constraint(equalTo: redView.widthAnchor, multiplier: 0.5).isActive = true
whiteView.heightAnchor.constraint(equalTo: redView.heightAnchor, multiplier: 0.5).isActive = true
```

### 範例05 
與其他 View 做比較，定義位置

[範例05](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter11/Example1105)

透過程式碼來定義

```Swift
let redView = UIView(frame: .zero)
redView.translatesAutoresizingMaskIntoConstraints = false
view.addSubview(redView)
redView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
redView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
redView.widthAnchor.constraint(equalToConstant: 300).isActive = true
redView.heightAnchor.constraint(equalToConstant: 300).isActive = true

let greenView = UIView(frame: .zero)
greenView.translatesAutoresizingMaskIntoConstraints = false
view.addSubview(greenView)
greenView.leftAnchor.constraint(equalTo: redView.leftAnchor).isActive = true
greenView.topAnchor.constraint(equalTo: redView.bottomAnchor).isActive = true
greenView.heightAnchor.constraint(equalToConstant: 100).isActive = true
greenView.widthAnchor.constraint(equalToConstant: 100).isActive = true

let blueView = UIView(frame: .zero)
blueView.translatesAutoresizingMaskIntoConstraints = false
view.addSubview(blueView)
blueView.rightAnchor.constraint(equalTo: redView.rightAnchor).isActive = true
blueView.topAnchor.constraint(equalTo: redView.bottomAnchor).isActive = true
blueView.heightAnchor.constraint(equalToConstant: 100).isActive = true
blueView.widthAnchor.constraint(equalToConstant: 100).isActive = true
```

### 範例06 
與其他 View 做比較，定義垂直或水平置中

[範例06](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter11/Example1106)

透過程式碼來定義

```Swift
let redView = UIView(frame: .zero)
view.addSubview(redView)
redView.translatesAutoresizingMaskIntoConstraints = false
redView.widthAnchor.constraint(equalToConstant: 300).isActive = true
redView.heightAnchor.constraint(equalToConstant: 300).isActive = true
redView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
redView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true

let greenView = UIView(frame: .zero)
view.addSubview(greenView)
greenView.translatesAutoresizingMaskIntoConstraints = false
greenView.widthAnchor.constraint(equalToConstant: 200).isActive = true
greenView.heightAnchor.constraint(equalToConstant: 200).isActive = true
greenView.centerXAnchor.constraint(equalTo: redView.centerXAnchor).isActive = true
greenView.centerYAnchor.constraint(equalTo: redView.centerYAnchor).isActive = true
```

### 範例07 
設置優先度

[範例07](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter11/Example1107)

透過程式碼來定義

```Swift
let myView = UIView(frame: .zero)
myView.translatesAutoresizingMaskIntoConstraints = false
myView.backgroundColor = .red
view.addSubview(myView)
myView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
myView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
myView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
myView.heightAnchor.constraint(equalToConstant: 150).isActive = true

//設置優先度
let heightAnchor = myView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1/4)
heightAnchor.priority = UILayoutPriority(rawValue: 750)
heightAnchor.isActive = true
```

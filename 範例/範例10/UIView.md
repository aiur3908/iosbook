# UIView

### 官方文件
[UIView](https://developer.apple.com/documentation/uikit/uiview)

### 建構函式

```Swift
let rect = CGRect(x: 10, y: 10, width: 100, height: 100)
let myView = UIView(frame: rect)
view.addSubview(myView)
```

### 透過建構函式產生UIView，並加入到畫面之內

```Swift
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let rect = CGRect(x: 10, y: 10, width: 100, height: 100)
        let myView = UIView(frame: rect)
        myView.backgroundColor = UIColor.red
        view.addSubview(myView)
    }

}
```

### 將子視圖從父視圖中移除

```Swift
myView.removeFromSuperview()
```

### 背景顏色

```Swift
myView.backgroundColor = UIColor.red
```

### 是否隱藏

```Swift
myView.isHidden = true
```
### 透明度

```Swift
myView.alpha = 0.5
```

### 辨識號碼

```Swift
myView.tag = 3
```

### 相對於父視圖的位置與大小

```Swift
myView.frame = CGRect(x: 20, y: 20, width: 100, height: 100)
```

### 父視圖，未加入到父視圖之前可能為空

```Swift
let superview = myView.superview
```

### 子視圖，可能有多個子視圖

```Swift
let subviews = myView.subviews
```

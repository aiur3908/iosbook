# UITextField

### 官方文件
[UITextField](https://developer.apple.com/documentation/uikit/uitextfield)

### 建構函式

```Swift
let rect = CGRect(x: 100, y: 100, width: 200, height: 50)
let myTextField = UITextField(frame: rect)
myTextField.borderStyle = .roundedRect
view.addSubview(myTextField)
```

### 顯示於UITextField的文字

```Swift
let text = myTextField.text
myTextField.text = "Hello World!"
```

### 邊框樣式

```Swift
myTextField.borderStyle = .none //無邊框
myTextField.borderStyle = .line //線段
myTextField.borderStyle = .bezel //邊框
myTextField.borderStyle = .roundedRect //圓角邊框
```
### 佔位符

```Swift
myTextField.placeholder = "帳號"
```

### 文字顏色

```Swift
myTextField.textColor = .red
```

### 文字字體

```Swift
myTextField.font = UIFont.systemFont(ofSize: 24)
```

### 背景顏色

```Swift
myTextField.backgroundColor = .green
```

### 是否為密碼

```Swift
myTextField.isSecureTextEntry = true
```

### 鍵盤種類

```Swift
myTextField.keyboardType = .numberPad
```

### 收起鍵盤

```Swift
view.endEditing(true)
```

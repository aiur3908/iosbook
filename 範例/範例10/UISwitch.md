# UISwitch

### 官方文件
[UISwitch](https://developer.apple.com/documentation/uikit/uiswitch)

### 建構函式

```Swift
let rect = CGRect(x: 100, y: 100, width: 50, height: 30)
let mySwitch = UISwitch(frame: rect)
view.addSubview(mySwitch)
```

### 範例01
UISwitch範例

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter10/UISwitch/UISwitchExample01)

### 用於偵測開關改變

```Swift
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let rect = CGRect(x: 100, y: 100, width: 50, height: 30)
        let mySwitch = UISwitch(frame: rect)
        view.addSubview(mySwitch)
        mySwitch.addTarget(self,
                           action: #selector(switchDidChanged(sender:)),
                           for: .valueChanged)
    }
    
    @objc func switchDidChanged(sender: UISwitch) {
        print(sender.isOn)
    }
}
```

### 是否開啟

```Swift
let isOn = mySwitch.isOn
```

### 切換開啟狀態，並且要求動畫效果

```Swift
mySwitch.setOn(true, animated: true)
```

### 開關的顏色，外圈的部分

```Swift
mySwitch.onTintColor = UIColor.red
```

### 圓球部分的顏色

```Swift
mySwitch.thumbTintColor = UIColor.blue
```

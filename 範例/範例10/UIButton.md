# UIButton

### 官方文件
[UIButton](https://developer.apple.com/documentation/uikit/uibutton)

### 建構函式

```Swift
let rect = CGRect(x: 10, y: 10, width: 100, height: 100)
let button = UIButton(frame: rect)
button.backgroundColor = UIColor.red
view.addSubview(button)
```

### 設置按鈕點擊後會觸發的函式

```Swift
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let rect = CGRect(x: 10, y: 10, width: 100, height: 100)
        let button = UIButton(frame: rect)
        button.backgroundColor = UIColor.red
        view.addSubview(button)
        button.addTarget(self,
                         action: #selector(ViewController.buttonTapped),
                         for: .touchUpInside)
    }
    
    @objc func buttonTapped() {
        print("點選按鈕了")
    }

}
```

### 根據狀態來設定標題文字與顏色

```Swift
myButton.setTitle("Hello", for: .normal)
myButton.setTitle("Hello!!", for: .highlighted)
myButton.setTitleColor(UIColor.red, for: .normal)
myButton.setTitleColor(UIColor.green, for: .highlighted)
```

### 根據狀態來設定按鈕圖片

```Swift
let image = UIImage(named: "icon_check")
myButton.setImage(image, for: .normal)
```

### 調整按鈕文字屬性

```Swift
myButton.titleLabel?.font = UIFont.systemFont(ofSize: 30)
```

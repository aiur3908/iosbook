# CGRect

### 官方文件
[CGRect](https://developer.apple.com/documentation/coregraphics/cgrect)

### 建構函式

```Swift
let rect = CGRect(x: 10, y: 10, width: 100, height: 100)
```

### 用於表示矩形的位置

```Swift
let rect = CGRect(x: 100, y: 100, width: 50, height: 50)
let origin = rect.origin
print(origin.x) // 100
print(origin.y) // 100
```
### 用於表示矩形的大小

```Swift
let rect = CGRect(x: 100, y: 100, width: 50, height: 50)
let size = rect.size
print(size.width) // 100
print(size.height) // 100
```
### Core Graphics官方文件

[Core Graphics官方文件](https://developer.apple.com/documentation/coregraphics)

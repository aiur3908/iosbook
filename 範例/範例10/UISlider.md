# UISlider

### 官方文件
[UISlider](https://developer.apple.com/documentation/uikit/uislider)

### 建構函式

```Swift
let rect = CGRect(x: 50, y: 100, width: 200, height: 50)
let mySlider = UISlider(frame: rect)
view.addSubview(mySlider)
```

### 設置最小與最大值

```Swift
//最小值
mySlider.minimumValue = 0
//最大值
mySlider.maximumValue = 1
```

### 得知或更改目前的值

```Swift
//得知當前的值
let value = mySlider.value
//設置當前的值
mySlider.value = 2
```

### 設置值，增加動畫效果

```Swift
mySlider.setValue(2, animated: true)
```

### 背景顏色

```Swift
mySlider.backgroundColor = UIColor.red
```

### 最小值區域的顏色

```Swift
mySlider.minimumTrackTintColor = UIColor.red
```

### 最大值區域的顏色

```Swift
mySlider.maximumTrackTintColor = UIColor.green
```

### 圓球的顏色

```Swift
mySlider.thumbTintColor = UIColor.blue
```

### 設置IBAction來偵測滑動事件

```Swift
@IBAction func didValueChanged(_ sender: UISlider) { 
    let value = sender.value
    print(" 使用者目前選擇的數值為 \(value)")
}
```

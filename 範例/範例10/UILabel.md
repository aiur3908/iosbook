# UILabel

### 官方文件
[UILabel](https://developer.apple.com/documentation/uikit/uilabel)

### 建構函式

```Swift
let rect = CGRect(x: 100, y: 100, width: 100, height: 100)
let myLabel = UILabel(frame: rect)
view.addSubview(myLabel)
```
### 顯示於UILabel的文字

```Swift
myLabel.text = "Hello"
```

### 文字顏色

```Swift
myLabel.textColor = UIColor.red
```

### 文字的字體與字體大小

```Swift
myLabel.font = UIFont.systemFont(ofSize: 16)
```

### 文字可以顯示的行數，若設置為0，則會無止盡換行

```Swift
myLabel.numberOfLines = 0
```

###  文字對齊的位置

```Swift
myLabel.textAlignment = .left //靠左
myLabel.textAlignment = .center //置中
myLabel.textAlignment = .right //靠右
```

### 陰影

```Swift
myLabel.shadowColor = UIColor.red
myLabel.shadowOffset = CGSize(width: 2, height: 2)
```

### 最小縮放比例

```Swift
myLabel.minimumScaleFactor = 0.5
```

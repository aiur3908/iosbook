# UIImageView

### 官方文件
[UIImageView](https://developer.apple.com/documentation/uikit/uiimageview)

### 建構函式

```Swift
let rect = CGRect(x: 100, y: 100, width: 100, height: 100)
let myImageView = UIImageView(frame: rect)
view.addSubview(myImageView)
```

### 設置圖片

```Swift
let homeImage = UIImage(named: "Home")
myImageView.image = homeImage
```

### 設置內容顯示模式

```Swift
myImageView.contentMode = .scaleToFill
```

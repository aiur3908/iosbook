# UITextView

### 官方文件
[UITextView](https://developer.apple.com/documentation/uikit/uitextview)

### 建構函式

```Swift
let rect = CGRect(x: 100, y: 100, width: 200, height: 200)
let myTextView = UITextView(frame: rect) 
myTextView.backgroundColor = UIColor.lightGray 
view.addSubview(myTextView)
```

### 顯示於UITextView的文字

```Swift
myTextView.text = "Hello"
print(myTextView.text!) //Hello
```
### 鍵盤的種類

```Swift
myTextView.keyboardType = .numberPad
```

### 文字顏色

```Swift
myTextView.textColor = .red
```

### 文字字體

```Swift
myTextView.font = UIFont.systemFont(ofSize: 24)
```

### 背景顏色

```Swift
myTextView.backgroundColor = .green
```

### 使用者是否可以選擇

```Swift
textView.isSelectable = false
```

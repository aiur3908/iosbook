# UIColor

### 官方文件
[UIColor](https://developer.apple.com/documentation/uikit/uicolor)

### 建構函式

```Swift
let color = UIColor(red: 255/255,
                    green: 0/255,
                    blue: 0/255,
                    alpha: 1)
```

### 預設顏色

```Swift
UIColor.red 
UIColor.green
UIColor.blue
UIColor.purple
UIColor.white
```

### 透過Assets儲存顏色並使用

```Swift
let color = UIColor(named: "MyColor")
```

# UISlider

### 官方文件
[UIStepper](https://developer.apple.com/documentation/uikit/uistepper)

### 建構函式

```Swift
let rect = CGRect(x: 50, y: 100, width: 100, height: 30)
let myStepper = UIStepper(frame: rect)
view.addSubview(myStepper)
```

###  可增減的最小值

```Swift
myStepper.minimumValue = 0
```

### 可增減的最大值

```Swift
myStepper.maximumValue = 100
```

### 得知或更改目前的值

```Swift
//得知當前的值
let value = myStepper.value
//設置當前的值
myStepper.value = 2
```

### 每次增減的值

```Swift
myStepper.stepValue = 2
```

### 可否按著遞增減

```Swift
myStepper.autorepeat = true
```

### 是否循環

```Swift
myStepper.wraps = true
```

### 透過IBAction來偵測改變時的事件

```Swift
@IBAction func didValueChanged(_ sender: UIStepper) { 
    let value = sender.value
    print(" 目前的數值為 \(value)")
}
```

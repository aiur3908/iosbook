# UISegmentedControl

### 官方文件
[UISegmentedControl](https://developer.apple.com/documentation/uikit/uisegmentedcontrol)

### 建構函式

```Swift
let items = ["貓", "狗", "海豚"]
let mySegmentedControl = UISegmentedControl(items: items)
let rect = CGRect(x: 50, y: 100, width: 200, height: 30)
mySegmentedControl.frame = rect
view.addSubview(mySegmentedControl)
```

### 得知或更改當前選擇的區段

```Swift
//得知當前區段
let index = mySegmentedControl.selectedSegmentIndex  
//更換當前區段
mySegmentedControl.selectedSegmentIndex = 1
```

### 設置選擇中的區段顏色

```Swift
mySegmentedControl.selectedSegmentTintColor = UIColor.red
```

### 背景顏色

```Swift
mySegmentedControl.backgroundColor = UIColor.green
```

### 得知該區段的標題

```Swift
let title = mySegmentedControl.titleForSegment(at: 0)
```

### 透過IBAction來得知切換區段的事件

```Swift
@IBAction func didValueChanged(_ sender: UISegmentedControl) {
    let index = sender.selectedSegmentIndex
    if let title = sender.titleForSegment(at: index) {
        print(" 使用者選擇了 \(title)")
   }
}
```

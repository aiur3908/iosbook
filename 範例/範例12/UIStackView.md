# UIStackView

### 官方文件
[UIStackView](https://developer.apple.com/documentation/uikit/uistackview)

### 透過程式碼產生UIStackView

```Swift
let stackView = UIStackView(frame: .zero)
view.addSubview(stackView)
stackView.translatesAutoresizingMaskIntoConstraints = false
stackView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
stackView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
stackView.heightAnchor.constraint(equalToConstant: 300).isActive = true
```

### 將子視圖加入至UIStackView，並且設置條件約束

```Swift
let redView = UIView(frame: .zero)
redView.backgroundColor = UIColor.red
stackView.addArrangedSubview(redView)

let greenView = UIView(frame: .zero)
greenView.backgroundColor = UIColor.green
stackView.addArrangedSubview(greenView)

let blueView = UIView(frame: .zero)
blueView.backgroundColor = UIColor.blue
stackView.addArrangedSubview(blueView)

redView.widthAnchor.constraint(equalToConstant: 100).isActive = true
greenView.widthAnchor.constraint(equalToConstant: 100).isActive = true
```

### 更改UIStackView的排列方向

```Swift
// 水平
stackView.axis = .horizontal 
// 垂直
stackView.axis = .vertical
```

### 視圖之間的間距

```Swift
stackView.spacing = 5
```

### 視圖的內容分配方式

```Swift
stackView.distribution = .fill
```

### 所有的排列視圖

```Swift
let arrangedSubviews = stackView.arrangedSubviews
```

### 增加排列視圖

```Swift
let myView = UIView(frame: .zero) 
stackView.addArrangedSubview(myView)
```

### 插入排列視圖

```Swift
let myView = UIView(frame: .zero)
stackView.insertArrangedSubview(myView, at: 1)
```

### 移除特定的排列視圖

```Swift
stackView.removeArrangedSubview(myView)
```

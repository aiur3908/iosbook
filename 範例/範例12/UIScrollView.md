# UIScrollView

### 官方文件
[UIStackView](https://developer.apple.com/documentation/uikit/uiscrollview)

### 透過程式碼產生UIStackView

```Swift
let scrollView = UIScrollView(frame: .zero)
view.addSubview(scrollView) 
scrollView.translatesAutoresizingMaskIntoConstraints = false 
scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
```

### 透過程式碼加入內容到UIScrollView之中

```Swift
let redView = UIView(frame: .zero)
redView.backgroundColor = .red
scrollView.addSubview(redView) redView.translatesAutoresizingMaskIntoConstraints = false
redView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
redView.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
redView.rightAnchor.constraint(equalTo: scrollView.rightAnchor).isActive = true
redView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
redView.heightAnchor.constraint(equalToConstant: 500).isActive = true

let blueView = UIView(frame: .zero)
blueView.backgroundColor = .blue
scrollView.addSubview(blueView)
blueView.translatesAutoresizingMaskIntoConstraints = false
blueView.topAnchor.constraint(equalTo: redView.bottomAnchor).isActive = true
blueView.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
blueView.rightAnchor.constraint(equalTo: scrollView.rightAnchor).isActive = true
blueView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
blueView.heightAnchor.constraint(equalToConstant: 500).isActive = true
blueView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
```

### 範例01
垂直捲動的UIScrollView

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter12/UIScrollView/UIScrollViewExample01)


### 範例02
水平捲動的UIScrollView

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter12/UIScrollView/UIScrollViewExample02)

### 範例03
縮放的UIScrollView

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter12/UIScrollView/UIScrollViewExample03)

程式碼
```Swift
class ViewController: UIViewController {
    
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.minimumZoomScale = 1
        scrollView.maximumZoomScale = 5
        scrollView.delegate = self
        setupGestureRecognizer()
    }
    
    private func setupGestureRecognizer() {
        let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        // 需要點兩下才會觸發
        doubleTapRecognizer.numberOfTapsRequired = 2
        // 於 UIScrollView 中增加手勢辨識
        scrollView.addGestureRecognizer(doubleTapRecognizer)
    }
    
    
    @objc func handleTap() {
        if scrollView.zoomScale > scrollView.minimumZoomScale {
            // 放到最小
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        } else {
            // 放到最大
            scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
        }
    }

}

extension ViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
```

# UITableView

### 官方文件
[UITableView](https://developer.apple.com/documentation/uikit/uitableview)

### 範例01
基本使用方式

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter16/UITableView/UITableViewExample01)

程式碼

```Swift
class ViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
    }

}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = "\(indexPath.row)"
        return cell
    }
}
```

### 範例02
設置Footer與Header

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter16/UITableView/UITableViewExample02)

程式碼

```Swift
class ViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
    }

}

extension ViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 2
        } else  {
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "Footer \(section)"
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Header \(section)"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = "\(indexPath.row)"
        return cell
    }
}
```

### 範例03
使用重用機制

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter16/UITableView/UITableViewExample03)

程式碼

```Swift
class ViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") else {
            fatalError("重用辨識碼錯誤")
        }
        cell.textLabel?.text = "\(indexPath.row)"
        return cell
    }
}
```

### 範例04
客製化UITableViewCell

[範例04](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter16/UITableView/UITableViewExample04)

程式碼

ViewController.swift

```Swift
class ViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
    }

}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? MyTableViewCell else {
            fatalError("重用辨識碼錯誤")
        }
        cell.myImageView.image = UIImage(named: "Apple")
        return cell
    }
}
```


MyTableViewCell.swit

```Swift
class MyTableViewCell: UITableViewCell {
    
    @IBOutlet var myImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
```

### 範例05
使用Xib來製作客製化UITableViewCell

[範例05](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter16/UITableView/UITableViewExample05)

程式碼

ViewController.swift

```Swift
class ViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    private func setupTableView() {
        let cellNib = UINib(nibName: "MyTableViewCell", bundle: .main)
        tableView.register(cellNib, forCellReuseIdentifier: "Cell")
        tableView.dataSource = self
    }

}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? MyTableViewCell else {
            fatalError("重用辨識碼錯誤")
        }
        
        cell.myImageView.image = UIImage(named: "Apple")
        return cell
    }
}
```

MyTableViewCell.swift

```Swift
class MyTableViewCell: UITableViewCell {

    @IBOutlet var myImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
```

## UITableViewDelegate

### heightForRowAt
Row的高度

```Swift
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
}
```

### heightForHeaderInSection
Header的高度

```Swift
func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 100
}
```

### heightForFooterInSection
Footer的高度

```Swift
func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 100
}
```

### viewForHeaderInSection
顯示於Header的View

```Swift
func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let headerView = UIView(frame: .zero)
    headerView.backgroundColor = UIColor.red
    return headerView
}
```

### viewForFooterInSection
顯示於Footer的View

```Swift
func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let footerView = UIView(frame: .zero)
    footerView.backgroundColor = UIColor.green
    return footerView
}
```

### 範例06
解決重用機制的問題

[範例06](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter16/UITableView/UITableViewExample06)

程式碼

ViewController.swift

```Swift
class ViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    var tableViewData: [Bool] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    private func setupTableView() {
        for _ in 0...15 {
            tableViewData.append(true)
        }
        
        tableView.dataSource = self
    }
    
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? MyTableViewCell else {
            fatalError("重用辨識碼錯誤")
        }
        
        cell.mySwitch.isOn = tableViewData[indexPath.row]
        cell.delegate = self
        return cell
    }
}

extension ViewController: MyTableViewCellDelegate {
    func myTableViewCell(_ myTableViewCell: MyTableViewCell, didChangeSwithIsOn isOn: Bool) {
        if let indexPath = tableView.indexPath(for: myTableViewCell) {
            tableViewData[indexPath.row] = isOn
        }
    }
}
```


MyTableViewCell.swift

```Swift
protocol MyTableViewCellDelegate: AnyObject {
    func myTableViewCell(_ myTableViewCell: MyTableViewCell,
                         didChangeSwithIsOn isOn: Bool)
}


class MyTableViewCell: UITableViewCell {
    
    @IBOutlet var mySwitch: UISwitch!
    weak var delegate: MyTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func switchDidValueChanged(_ sender: UISwitch) {
        delegate?.myTableViewCell(self, didChangeSwithIsOn: sender.isOn)
    }

}
```

### reloadData
重新整理所有的Section與Row

```Swift
tableView.reloadData()
```

### reloadSections
重整指定的Section們，並且設定動畫效果

```Swift
tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
```

### reloadRows
重整指定的Row們，並且設定動畫效果

```Swift
tableView.reloadRows(at: [IndexPath(row: 0, section: 0), 
                          IndexPath(row: 1, section: 0),
                          IndexPath(row: 2, section: 0)], 
                   with: .automatic)
```

### 範例07
Static Cell

[範例07](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter16/UITableView/UITableViewExample07)

程式碼

```Swift
class MyTableViewController: UITableViewController {

    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var addressTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        let name = nameTextField.text ?? ""
        let phone = phoneTextField.text ?? ""
        let address = addressTextField.text ?? ""
        print("姓名: \(name)")
        print("電話: \(phone)")
        print("地址: \(address)")
    }

}
```

# Chapter 4

### 基本運算子

```Swift
let value1 = 1 + 1 // 1+1 = 2
let value2 = 3 - 1 // 3-1 = 2 
let value3 = 3 * 3 // 3*3 = 9
let value4 = 4 / 2 // 4/2 = 2
```

### 先乘除後加減與括弧更換計算順序

```Swift
let value1 = 1 + 2 * 4 // = 9
let value2 = (1 + 2) * 4 // = 12
```

### 透過加號運算子來合併字串與陣列

```Swift
let string = "Hello" + "World" // HelloWorld
let array = [0, 1, 2] + [3, 4] // [0, 1, 2, 3, 4]
```

### 餘數運算

```Swift
let value1 = 4 % 3 //1 
let value2 = 4 % 4 //0
```

### 透過轉型來進行加法運算

```Swift
let a: Int = 3
let b: Double = 1.5
print(Double(a) + b)
```

### 加號賦值運算子

```Swift
var a = 1
a += 1 // a = 2
```

### 減號賦值運算子

```Swift
var b = 1
b -= 1  // b = 0
```

### 布林值宣告

```Swift
var bool1: Bool = true 
var bool2: Bool = false
```

### 透過型別推斷來宣告布林值

```Swift
var bool1 = true
```

### 透過關係運算子來取得對應的布林值

```Swift
let a = 10
let b = 13
let boolValue1 = a == b  //false
let boolValue2 = a != b  //true
let boolValue3 = a > b   //false
let boolValue4 = a < b   //true
let boolValue5 = a >= b  //false
let boolValue6 = a <= b  //true
```

###  隨機產生一個布林值

```Swift
let bool = Bool.random()
```

### 反轉當前的布林值

```Swift
var bool = true
bool.toggle() // false
```
### 判別該字串是否有包含某些文字

```Swift
var string = "Hello World"
var bool = string.contains("Hello") //true
```

### 判別該陣列是否有包含某個元素

```Swift
var array = [1, 2, 3]
var bool = array.contains(1) //true
```

### 條件判斷式

```Swift
let a = 10

if a > 0 {
    //條件成立
    print("a > 0")
} else {
    //條件不成立
    print("a <= 0")
}
```

### 條件判斷式

```Swift
let a = 10

if a > 10 {
    print("a比10大")
} else if a > 5 {
    print("a比5大")
} else {
    print("其他")
}
```

### 沒有else的條件判斷式

```Swift
let a = 10 
if a > 10 {
    print("a 比 10 大 ")
}
```

### 直接使用布林值來進行條件判斷

```Swift
let a = 10
let bool = a > 10 
if bool {
    print("a比10大")
}
```

### 透過或(Or)來進行條件判斷

```Swift
let a = -11
if a > 10 || a < -10 {
    print("a的絕對值 > 10")
}
```
### 透過和(And)來進行條件判斷

```Swift
let gender = "男"
let age = 18
if gender == "男" && age >= 18 {
    print("需要當兵了") 
}
```

### 巢狀條件判斷式

```Swift
if bool1 {
    if bool2 {
    
    }
}
```

### 三元運算子

```Swift
var isPhoneX = true
let height = isPhoneX ? 100 : 50
```

### 透過for-in來迭代陣列內的元素

```Swift
let names = ["Anna", "Alex", "Brian", "Jack"] 
for name in names {
    print("Hello, \(name)!") 
}
//Hello, Anna! 
//Hello, Alex! 
//Hello, Brian! 
//Hello, Jack!
```

### 透過for-in來迭代數字範圍

```Swift
for index in 1...5 { 
    print(index)
}
//1
//2
//3 
//4 
//5
```

### 起始值與結束值相同的迴圈

```Swift
for index in 1...1 {   
    print(index)
}
```
### 將數字範圍改成小於符號

```Swift
/// 1 ~ 小於5 等同於 1...4 
for index in 1..<5 {
    print(index)
}
//1 
//2 
//3 
//4
```

### 忽略迭代的值

```Swift
for _ in 1...5 {    
    print("Hello")
}
```

### 透過for-in 將字典內的值取出

```Swift
let dictionary = ["A": 1, "B": 2, "C": 3]
for (key, value) in dictionary {
    print("key = \(key), value = \(value)") 
}
//key = C, value = 3 
//key = A, value = 1 
//key = B, value = 2
```

### stride(from: to: by:)

```Swift
// 從 3 到 15 每次 +3，當值達到 15 則結束，但是剛好等於 15 不會印出
for index in stride(from: 3, to: 15, by: 3) {
    print(index)
}
//3 
//6 
//9
//12
```

### stride(from: through: by:)

```Swift
/// 從 3 到 15 每次 +3，當值達到 15 則結束，剛好等於 15 會印出
for index in stride(from: 3, through: 15, by: 3) {
    print(index) 
}
//3
//6 
//9 
//12 
//15
```

### while 迴圈

```Swift
var index = 0
while index < 5 {
    index = index + 1
    print(index) 
}
```

### repeat-while迴圈

```Swift
var index = 0
repeat {
    index = index + 1 
    print(index)
} while index < 0
```
### while 迴圈

```Swift
var index = 0
while index < 0 { 
    index = index + 1 
    print(index)
}
```

### 列舉的宣告與使用

```Swift
enum GenderType { 
    case male
    case female 
}

//宣告一個列舉的變數
let gender: GenderType = GenderType.male
//直接透過點來指定列舉
let gender: GenderType = .male
//透過型別推斷來宣告列舉
let gender = GenderType.male
```

### 使用列舉

```Swift
let gender = GenderType.male
if gender == .male {
    print(" 男生 ")
} else if gender == .female { 
    print(" 女生 ")
}
```

### 宣告多個值於列舉之中

```Swift
enum CompassPoint {
    case north, south, east, west
}
```

### 透過switch case來判別列舉選項 - 有default值的狀況下

```Swift
let directionToHead = CompassPoint.north 
switch directionToHead {
    case .north:
        print(" 往北 ") 
    default:
        print(" 其他 ") 
}
```

### 透過switch case來判別列舉選項 - 擁有全部條件的狀況下

```Swift
let directionToHead = CompassPoint.north 
switch directionToHead {
    case .north:
        print(" 往北 ")
    case .south:
        print(" 往南 ")
    case .east:
        print(" 往東 ") 
    case .west:
        print(" 往西 ")
}
```

### 定義列舉的原始值

```Swift
enum Weekday: Int { 
    case monday = 1
    case tuesday = 2 
    case wednesday = 3 
    case thursday = 4 
    case friday = 5 
    case saturday = 6 c
    ase sunday = 7
}          
```

### 透過原始值來產生對應的列舉內容

```Swift
let monday = Weekday(rawValue: 1)!
```

### 列舉原始值與列舉名稱相同時，可省略原始值資料

```Swift
enum CompassPoint: String { 
    case north
    case south 
    case east 
    case west
}

let north = CompassPoint.north 
print(north.rawValue) //north
```

### 自動遞增的原始值

```Swift
enum Weekday: Int {
    case monday = 1
    case tuesday 
    case wednesday 
    case thursday 
    case friday 
    case saturday 
    case sunday
}

let sunday = Weekday.sunday
print(sunday.rawValue) //7
```
